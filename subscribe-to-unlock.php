<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
  Plugin Name: Subscribe To Unlock
  Plugin URI:  http://demo.wpshuffle.com/subscribe-to-unlock/
  Description: A plugin to collect email of your site visitors by locking certain content of your site until visitors subscribes through the subscription forms
  Version:     1.1.5
  Author:      WP Shuffle
  Author URI:  http://wpshuffle.com
  Domain Path: /languages
  Text Domain: subscribe-to-unlock
 */



/**
 * Plugin's main class
 */
if ( !class_exists( 'STU_Class' ) ) {

    class STU_Class {

        function __construct() {
            $this->define_constants();
            $this->includes();
        }

        /**
         * Necessary constants
         */
        function define_constants() {

            global $wpdb;
            defined( 'STU_VERSION' ) or define( 'STU_VERSION', '1.1.5' ); // Plugin's active version
            defined( 'STU_PATH' ) or define( 'STU_PATH', plugin_dir_path( __FILE__ ) ); // plugin's absolute path
            defined( 'STU_URL' ) or define( 'STU_URL', plugin_dir_url( __FILE__ ) ); // plugin's absolute path
            defined( 'STU_IMG_DIR' ) or define( 'STU_IMG_DIR', plugin_dir_url( __FILE__ ) . '/images' ); // plugin's image directory URL
            defined( 'STU_CSS_DIR' ) or define( 'STU_CSS_DIR', plugin_dir_url( __FILE__ ) . '/css' ); // plugin's image directory URL
            defined( 'STU_JS_DIR' ) or define( 'STU_JS_DIR', plugin_dir_url( __FILE__ ) . '/js' ); // plugin's image directory URL
            defined( 'STU_TD' ) or define( 'STU_TD', 'subscribe-to-unlock' ); //plugin's translation text domain
            defined( 'STU_FORM_TABLE' ) or define( 'STU_FORM_TABLE', $wpdb->prefix . 'stu_forms' ); //form table name
            defined( 'STU_SUBSCRIBERS_TABLE' ) or define( 'STU_SUBSCRIBERS_TABLE', $wpdb->prefix . 'stu_subscribers' ); //plugin's subscriber table
            defined( 'STU_MC_API_URL' ) or define( 'STU_MC_API_URL', 'https://api.mailchimp.com/3.0/' ); //Mailchimp default api url
            defined( 'STU_CC_API_URL' ) or define( 'STU_CC_API_URL', 'https://api.constantcontact.com/v2/' ); //Constant Contact default api url
            defined( 'STU_TOTAL_TEMPLATES' ) or define( 'STU_TOTAL_TEMPLATES', 20 ); //Total number of templates available
            defined( 'STU_LANGUAUGE_PATH' ) or define( 'STU_LANGUAUGE_PATH', basename( dirname( __FILE__ ) ) . '/languages/' );
        }

        /**
         * Includes necessary classes & files
         */
        function includes() {
            include(STU_PATH . 'inc/classes/class-stu-library.php');
            include(STU_PATH . 'inc/classes/class-stu-init.php');
            include(STU_PATH . 'inc/classes/class-stu-activation.php');
            include(STU_PATH . 'inc/classes/class-stu-admin.php');
            include(STU_PATH . 'inc/classes/class-stu-enqueue.php');
            include(STU_PATH . 'inc/classes/class-stu-ajax-admin.php');
            include(STU_PATH . 'inc/classes/class-stu-ajax.php');
            include(STU_PATH . 'inc/classes/class-stu-shortcode.php');
            include(STU_PATH . 'inc/classes/class-stu-downloader.php');
            include(STU_PATH . 'inc/classes/class-stu-hooks.php');
            include(STU_PATH . 'inc/classes/class-stu-metabox.php');
            if ( is_admin() ) {
                include(STU_PATH . 'inc/classes/class-stu-tinymce.php');
            }
            include(STU_PATH . 'inc/classes/class-stu-mobile-detect.php');
        }

    }

    $stu_obj = new STU_Class(); //initialization of plugin
}