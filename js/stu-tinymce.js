jQuery(function($){
    "use strict";
    tinymce.PluginManager.add('stu_shortcode_generator', function (editor, url) {

        // Add Button to Visual Editor Toolbar
        editor.addButton('stu_shortcode_generator', {
            title: $('#stu-tinymce-icon-title').val(),
            cmd: 'stu_shortcode_generator',
            image: $('#stu-tinymce-icon').val()
        });

        editor.addCommand('stu_shortcode_generator', function () {
            // Check we have selected some text that we want to link
            var text = editor.selection.getContent({
                'format': 'html'
            });
            if (text.length === 0) {
                alert($('#stu-tinymce-error-message').val());
                return;
            }
            $('.stu-tinymce-popup').fadeIn(500);
            $('body').on('click', '.stu-shortcode-inserter', function () {
                var form_alias = $('#stu-form-lists').val();
                if (form_alias != '') {
                    editor.execCommand('mceReplaceContent', false, '[stu alias="' + form_alias + '"]'+text+'[/stu]');
                    $('.stu-tinymce-popup').fadeOut(500);
                    editor.selection.clear();
                }
            });
        });
    });
    $('body').on('click','.stu-tinymce-popup-close',function(){
        $('.stu-tinymce-popup').fadeOut(500);
    });

});