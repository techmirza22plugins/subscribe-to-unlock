<?php

defined('ABSPATH') or die('No script kiddies please!!');
if (!class_exists('STU_Metabox')) {

    class STU_Metabox extends STU_Library {

        function __construct() {
            add_action('add_meta_boxes', array($this, 'add_stu_metabox'));
            add_action('save_post', array($this, 'save_stu_metabox'), 10, 2);
        }

        function add_stu_metabox() {
            add_meta_box('stu-metabox', esc_html__('Subscribe to Unlock', 'subscribe-to-unlock'), array($this, 'render_stu_metabox'), array(), 'side', 'default');
        }

        function render_stu_metabox($post) {
            include(STU_PATH . 'inc/views/backend/stu-metabox.php');
        }

        /**
         * Handles saving the meta box.
         *
         * @param int     $post_id Post ID.
         * @param WP_Post $post    Post object.
         * @return null
         *
         * @since 1.0.0
         */
        public function save_stu_metabox($post_id, $post) {

            // Check if nonce is valid.
            if (empty($_POST['stu_metabox_nonce_field'])) {
                return;
            }
            if (!wp_verify_nonce($_POST['stu_metabox_nonce_field'], 'stu_metabox_nonce')) {
                return;
            }

            // Check if user has permissions to save data.
            if (!current_user_can('edit_post', $post_id)) {
                return;
            }

            // Check if not an autosave.
            if (wp_is_post_autosave($post_id)) {
                return;
            }

            // Check if not a revision.
            if (wp_is_post_revision($post_id)) {
                return;
            }

            if (empty($_POST['stu_metabox_details'])) {
                return;
            }

            $stu_metabox_details = $this->sanitize_array($_POST['stu_metabox_details']);
            update_post_meta($post_id, '_stu_metabox_details', $stu_metabox_details);
        }

    }

    new STU_Metabox();
}
