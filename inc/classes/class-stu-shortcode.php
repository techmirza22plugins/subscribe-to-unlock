<?php
defined('ABSPATH') or die('No script kiddies please!!');
if (!class_exists('STU_Shortcode')) {

    class STU_Shortcode extends STU_Library {

        function __construct() {
            add_shortcode('stu', array($this, 'generate_shortcode_output'));
            add_action('wp_footer', array($this, 'append_subscription_form'));
        }

        function generate_shortcode_output($atts, $content = null) {
            wp_enqueue_style('stu-frontend-custom', STU_CSS_DIR . '/stu-custom.css', array(), STU_VERSION);
            if (isset($atts['alias'])) {
                $alias = $atts['alias'];
                $form_row = $this->get_form_row_by_alias($alias);
                if (!empty($form_row) && $form_row->form_status == 1) {
                    ob_start();
                    include(STU_PATH . 'inc/views/frontend/stu-shortcode.php');
                    $form_html = ob_get_contents();
                    ob_clean();
                    ob_end_flush();
                    return $form_html;
                }
            } else {
                return '';
            }
        }

        function append_subscription_form() {
            $unlock_check = (!isset($_COOKIE['stu_unlock_key'], $_COOKIE['stu_unlock_check'])) ? true : false;
            $stu_settings = get_option('stu_settings');
            $unlock_check = (!empty($stu_settings['extra']['test_mode']) && is_user_logged_in()) ? true : $unlock_check;

            if (!isset($_GET['stu_preview']) && $unlock_check) {
                wp_reset_query();
                global $wp_query;
                if (!empty($wp_query->queried_object->ID)) {
                    $stu_metabox_details = get_post_meta($wp_query->queried_object->ID, '_stu_metabox_details', true);
                    if (!empty($stu_metabox_details['enable_popup']) && !empty($stu_metabox_details['form_alias'])) {
                        $countdown_timer = $stu_metabox_details['countdown_timer'];
                        $countdown_timer_message = $stu_metabox_details['countdown_timer_message'];
                        $form_alias = $stu_metabox_details['form_alias'];
                        $popup_display_type = (!empty($stu_metabox_details['display_type'])) ? $stu_metabox_details['display_type'] : 'countdown';
                        $popup_display_class = 'stu-' . $popup_display_type . '-popup';
                        $delay_time = (!empty($stu_metabox_details['delay_time'])) ? $stu_metabox_details['delay_time'] : 0;
                        ?>
                        <div class="stu-popup-innerwrap <?php echo esc_attr($popup_display_class); ?>"
                        <?php
                        if ($popup_display_type == 'countdown') {
                            ?>
                                 data-countdown-timer="<?php echo intval($countdown_timer); ?>"
                                 <?php
                             }
                             if ($popup_display_type == 'delay') {
                                 ?>
                                 data-delay-time="<?php echo intval($delay_time); ?>"
                             <?php }
                             ?>

                             >
                            <div class="stu-overlay stu-popup-wrapper">
                                <div class="stu-popup-content-wrap">
                                    <?php echo do_shortcode('[stu alias="' . esc_attr($form_alias) . '" page_lock="yes"]');
                                    ?>
                                </div>
                                <?php if (!empty($countdown_timer) && $popup_display_type == 'countdown') { ?>
                                    <div class="stu-countdown-timer-message"><?php echo $this->sanitize_html(str_replace('#countdown', '<span class="stu-countdown-number">' . intval($countdown_timer) . '</span>', $countdown_timer_message)); ?></div>
                                <?php } ?>
                            </div>
                        </div>

                        <?php
                    }
                } else {
                    if (is_home()) {
                        $stu_settings = get_option('stu_settings');
                        if (!empty($stu_settings['extra']['lock_index_page']) && !empty($stu_settings['extra']['form_alias'])) {
                            $countdown_timer = $stu_settings['extra']['countdown_timer'];
                            $countdown_timer_message = $stu_settings['extra']['countdown_timer_message'];
                            $form_alias = $stu_settings['extra']['form_alias'];
                            ?>
                            <div class="stu-popup-innerwrap stu-countdown-popup" data-countdown-timer="<?php echo intval($countdown_timer); ?>">
                                <div class="stu-overlay stu-popup-wrapper">
                                    <div class="stu-popup-content-wrap">
                                        <?php echo do_shortcode('[stu alias="' . esc_attr($form_alias) . '" page_lock="yes"]');
                                        ?>
                                    </div>
                                    <?php if (!empty($countdown_timer)) { ?>
                                        <div class="stu-countdown-timer-message"><?php echo $this->sanitize_html(str_replace('#countdown', '<span class="stu-countdown-number">' . intval($countdown_timer) . '</span>', $countdown_timer_message)); ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
            }
            ?>
            <svg id="svg-filter">
            <filter id="svg-blur">
                <feGaussianBlur in="SourceGraphic" stdDeviation="12"></feGaussianBlur>
            </filter>
            </svg>
            <?php
        }

    }

    new STU_Shortcode();
}
