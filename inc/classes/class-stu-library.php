<?php

defined('ABSPATH') or die('No script kiddies please!!');
if (!class_exists('STU_Library')) {

    class STU_Library {

        /**
         * Exit for unauthorized access
         *
         * @since 1.0.0
         */
        function permission_denied() {
            die('No script kiddies please!!');
        }

        /**
         * Prints array in the pre format
         *
         * @param array $array
         * @since 1.0.0
         */
        function print_array($array) {
            echo "<pre>";
            print_r($array);
            echo "</pre>";
        }

        /**
         * Sanitizes Multi Dimensional Array
         * @param array $array
         * @param array $sanitize_rule
         * @return array
         *
         * @since 1.0.0
         */
        function sanitize_array($array = array(), $sanitize_rule = array()) {
            if (!is_array($array) || count($array) == 0) {
                return array();
            }

            foreach ($array as $k => $v) {
                if (!is_array($v)) {

                    $default_sanitize_rule = (is_numeric($k)) ? 'html' : 'text';
                    $sanitize_type = isset($sanitize_rule[$k]) ? $sanitize_rule[$k] : $default_sanitize_rule;
                    $array[$k] = $this->sanitize_value($v, $sanitize_type);
                }
                if (is_array($v)) {
                    $array[$k] = $this->sanitize_array($v, $sanitize_rule);
                }
            }

            return $array;
        }

        /**
         * Sanitizes Value
         *
         * @param type $value
         * @param type $sanitize_type
         * @return string
         *
         * @since 1.0.0
         */
        function sanitize_value($value = '', $sanitize_type = 'text') {
            switch ($sanitize_type) {
                case 'html':
                    $allowed_html = wp_kses_allowed_html('post');
                    return $this->sanitize_html($value);
                    break;
                case 'to_br':
                    return $this->sanitize_escaping_linebreaks($value);
                    break;
                case 'none':
                    return $value;
                    break;
                default:
                    return sanitize_text_field($value);
                    break;
            }
        }

        /**
         * Check if alias has already been used or not
         *
         * @param string $form_alias
         * @param int $form_id
         *
         * @since 1.0.0
         */
        function is_alias_available($form_alias, $form_id = 0) {
            $form_table = STU_FORM_TABLE;
            global $wpdb;
            if (empty($form_id)) {
                $alias_count = $wpdb->get_var("SELECT COUNT(*) FROM $form_table WHERE form_alias like '$form_alias'");
            } else {
                $alias_count = $wpdb->get_var("SELECT COUNT(*) FROM $form_table WHERE form_alias like '$form_alias' AND form_id !=$form_id");
            }
            if ($alias_count == 0) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Ajax nonce verification for ajax in admin
         *
         * @return bolean
         * @since 1.0.0
         */
        function admin_ajax_nonce_verify() {
            if (!empty($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'stu_ajax_nonce')) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Ajax nonce verification for ajax in frontend
         *
         * @return bolean
         * @since 1.0.0
         */
        function ajax_nonce_verify() {
            if (!empty($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'stu_frontend_ajax_nonce')) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Fetches the form row from form table
         *
         * @global object $wpdb
         * @param int $form_id
         * @return object
         */
        public static function get_form_row_by_id($form_id) {
            global $wpdb;
            $table = STU_FORM_TABLE;
            $form_row = $wpdb->get_row($wpdb->prepare("select * from $table where form_id = %d", $form_id));
            return $form_row;
        }

        /**
         * Returns the shortcode from alias
         *
         * @param string $alias
         * @return string
         *
         * @since 1.0.0
         */
        function generate_shortcode($alias) {
            $shortcode = "[stu alias='$alias' ]";
            return $shortcode;
        }

        /**
         * Returns the default email message
         *
         * @return string
         *
         * @since 1.0.0
         */
        function get_default_email_message() {
            $default_email_message = esc_html__(sprintf('Hello There,

Thank you for subscribing in our %s website. Please click on below link to unlock the content:

#unlock_link

Thank you', esc_attr(get_bloginfo('name'))), 'subscribe-to-unlock');
            return $default_email_message;
        }

        /**
         * Returns the default unlock link message
         *
         * @return string
         *
         * @since 1.0.0
         */
        function get_default_unlock_link_message() {
            $default_unlock_link_message = esc_html__('Congratulations!!

Your email has been verified and the locked content has been unlocked.

Please refresh the previous page to view the locked content.

Thank you', 'subscribe-to-unlock');
            return $default_unlock_link_message;
        }

        /**
         * Sanitizes the content by bypassing allowed html
         *
         * @param string $text
         * @return string
         *
         * @since 1.0.0
         */
        function sanitize_html($text, $br_omit = false) {
            $allowed_html = wp_kses_allowed_html('post');
            if ($br_omit) {
                unset($allowed_html['br']);
            }
            return wp_kses($text, $allowed_html);
        }

        /**
         * Sanitizes field by converting line breaks to <br /> tags
         *
         * @since 1.0.0
         *
         * @return string $text
         */
        function sanitize_escaping_linebreaks($text) {
            $text = $this->sanitize_html($text, true);
            $text = implode("<br \>", explode("\n", $text));
            return $text;
        }

        /**
         * Outputs by converting <Br/> tags into line breaks
         *
         * @since 1.0.0
         *
         * @return string $text
         */
        function output_converting_br($text) {
            $text = $this->sanitize_html($text, true);
            $text = implode("\n", explode("<br \>", $text));
            return $text;
        }

        /**
         * Gets the form row as per the alias
         *
         * @param string $alias
         *
         * @return object
         *
         * @since 1.0.0
         */
        function get_form_row_by_alias($alias) {
            global $wpdb;
            $form_table = STU_FORM_TABLE;
            $form_row = $wpdb->get_row("select * from $form_table where form_alias = '$alias'");
            return $form_row;
        }

        /**
         * Gets the subscriber row from email
         *
         * @param string $email
         *
         * @return object/boolean
         *
         * @since 1.0.0
         */
        function get_subscriber_row_by_email($email, $verified_check = false) {
            global $wpdb;
            $subscriber_table = STU_SUBSCRIBERS_TABLE;
            if ($verified_check) {
                $subscriber_row = $wpdb->get_row($wpdb->prepare("select * from $subscriber_table where subscriber_email = %s and subscriber_verification_status = 1", $email));
            } else {
                $subscriber_row = $wpdb->get_row($wpdb->prepare("select * from $subscriber_table where subscriber_email = %s", $email));
            }

            return $subscriber_row;
        }

        /**
         * Returns the default From Email
         *
         * @return string
         *
         * @since 1.0.0
         */
        function get_default_from_email() {
            $site_url = site_url();
            $find_h = '#^http(s)?://#';
            $find_w = '/^www\./';
            $replace = '';
            $output = preg_replace($find_h, $replace, $site_url);
            $output = preg_replace($find_w, $replace, $output);
            return 'noreply@' . $output;
        }

        /**
         * Generates a unique unlock key
         *
         * @return string
         *
         * @since 1.0.0
         */
        function generate_unlock_key() {
            $current_date_time = date('Y-m-d H:i:s');
            $unlock_key = md5($current_date_time);
            return $unlock_key;
        }

        /**
         * Returns unlock link message
         *
         * @global object $wpdb
         * @param string $unlock_key
         * @return string/boolean
         *
         * @since 1.0.0
         */
        function get_unlock_link_message($unlock_key) {
            $subscriber_table = STU_SUBSCRIBERS_TABLE;
            global $wpdb;
            $subscriber_row = $wpdb->get_row($wpdb->prepare("select * from $subscriber_table where subscriber_unlock_key = %s", $unlock_key));
            if (empty($subscriber_row)) {
                // if unlock key is inavalid
                return false;
            }
            $form_alias = $subscriber_row->subscriber_form_alias;
            $form_row = $this->get_form_row_by_alias($form_alias);
            if (empty($form_row)) {
                //if form has been deleted or not available in db
                return false;
            }
            $form_details = maybe_unserialize($form_row->form_details);

            $unlock_link_message = $form_details['general']['unlock_link_message'];
            return $unlock_link_message;
        }

        /**
         * Returns the file name from download URL
         *
         * @param string $url
         * @return string
         *
         * @since 1.0.0
         */
        function get_file_name_from_url($url) {
            $url = untrailingslashit($url);
            $url_array = explode('/', $url);
            $file_name = end($url_array);
            return $file_name;
        }

        /**
         * Copy form
         *
         * @param int $form_id
         * @return boolean
         *
         * @since 1.0.0
         */
        function copy_form($form_id) {
            global $wpdb;
            $form_row = $this->get_form_row_by_id($form_id);
            $form_alias = $form_row->form_alias . '_' . rand(1111, 9999);
            $copy_check = $wpdb->insert(
                    STU_FORM_TABLE, array(
                'form_title' => $form_row->form_title . ' - Copy',
                'form_alias' => $form_alias,
                'form_details' => $form_row->form_details,
                'form_status' => $form_row->form_status
                    ), array('%s', '%s', '%s', '%d'));
            return $copy_check;
        }

        function check_if_already_subscribed($unlock_key) {
            $unlock_key = sanitize_text_field($unlock_key);
            global $wpdb;
            $subscriber_table = STU_SUBSCRIBERS_TABLE;
            $subscriber_count = $wpdb->get_var("SELECT COUNT(*) FROM $subscriber_table WHERE subscriber_unlock_key like '$unlock_key'");
            if ($subscriber_count == 0) {
                return false;
            } else {
                return true;
            }
        }

        /**
         * Prints Display None
         *
         * @param string $parameter1
         * @param string $parameter2
         *
         * @since 1.0.0
         */
        function display_none($parameter1, $parameter2) {
            if ($parameter1 != $parameter2) {
                echo 'style="display:none"';
            }
        }

        function get_mc_headers($api_key) {
            global $wp_version;

            $headers = array();
            $headers['Authorization'] = 'Basic ' . base64_encode('std:' . $api_key);
            $headers['Accept'] = 'application/json';
            $headers['Content-Type'] = 'application/json';
            $headers['User-Agent'] = 'std/' . STU_VERSION . '; WordPress/' . $wp_version . '; ' . get_bloginfo('url');



            return $headers;
        }

        function get_cc_headers($access_token) {
            global $wp_version;

            $headers = array();
            $headers['Authorization'] = 'Bearer ' . $access_token;
            $headers['Accept'] = 'application/json';
            $headers['Content-Type'] = 'application/json';
            $headers['User-Agent'] = 'std/' . STU_VERSION . '; WordPress/' . $wp_version . '; ' . get_bloginfo('url');
            return $headers;
        }

        function mailchimp_api_connection($api_url, $api_key) {
            $api_url = STU_MC_API_URL;
            $dash_position = strpos($api_key, '-');
            if ($dash_position !== false) {
                $api_url = str_replace('//api.', '//' . substr($api_key, $dash_position + 1) . ".api.", $api_url);
                $args = array(
                    'url' => $api_url,
                    'method' => $method,
                    'headers' => $this->get_mc_headers($api_key),
                    'timeout' => 10,
                    'sslverify' => apply_filters('stu_mc_use_sslverify', true),
                );
                $api_url = add_query_arg(array('fields' => 'account_id'), $api_url);
                $mailchimp_connection = wp_remote_get($api_url, $args);
            }
        }

        /**
         * Subscribe the user to mailchimp list
         *
         * @param string $list_id
         * @param array $post_parameters
         * @return null
         */
        function subscribe_to_mailchimp($list_id, $post_parameters) {
            $api_key = $this->get_mailchimp_api_key();
            if (empty($api_key)) {
                return;
            }
            $api_url = STU_MC_API_URL;
            $dash_position = strpos($api_key, '-');
            if ($dash_position !== false) {
                $api_url = str_replace('//api.', '//' . substr($api_key, $dash_position + 1) . ".api.", $api_url);
                $api_url = $api_url . 'lists/' . $list_id;
                $method = 'POST';
                $args = array(
                    'url' => $api_url,
                    'headers' => $this->get_mc_headers($api_key),
                    'timeout' => 10,
                    'sslverify' => true,
                    'body' => json_encode($post_parameters)
                );
                $mailchimp_connection = wp_remote_post($api_url, $args);


                $this->record_mc_response(wp_remote_retrieve_body($mailchimp_connection));
            }
        }

        function get_mailchimp_api_key() {
            $stu_settings = get_option('stu_settings');
            return (!empty($stu_settings['mailchimp']['api_key'])) ? $stu_settings['mailchimp']['api_key'] : '';
        }

        function record_mc_response($raw_response) {
            $stu_settings = get_option('stu_settings');
            $stu_settings['mailchimp']['mc_log'] = $this->sanitize_html($raw_response);
            update_option('stu_settings', $stu_settings);
        }

        /**
         * Subscribe the user to constant contact list
         *
         * @param string $list_id
         * @param array $post_parameters
         * @return null
         */
        function subscribe_to_cc($list_id, $post_parameters) {
            $stu_settings = get_option('stu_settings');
            if (empty($stu_settings['constant_contact']['api_key']) || empty($stu_settings['constant_contact']['access_token'])) {
                return;
            }
            $api_url = STU_CC_API_URL;
            $api_key = $stu_settings['constant_contact']['api_key'];
            $access_token = $stu_settings['constant_contact']['access_token'];
            $api_url = $api_url . 'contacts?action_by=ACTION_BY_OWNER&api_key=' . $api_key;
            $method = 'POST';
            $args = array(
                'url' => $api_url,
                'headers' => $this->get_cc_headers($access_token),
                'timeout' => 10,
                'sslverify' => true,
                'body' => json_encode($post_parameters)
            );
            $contant_contact_connection = wp_remote_post($api_url, $args);
            $this->record_cc_response(wp_remote_retrieve_body($contant_contact_connection));
            return $contant_contact_connection;
        }

        function record_cc_response($raw_response) {
            $stu_settings = get_option('stu_settings');
            $stu_settings['constant_contact']['cc_log'] = $this->sanitize_html($raw_response);
            update_option('stu_settings', $stu_settings);
        }

        function change_verification_status($unlock_key) {
            global $wpdb;
            $wpdb->update(STU_SUBSCRIBERS_TABLE, array('subscriber_verification_status' => 1), array('subscriber_unlock_key' => $unlock_key), array('%d'), array('%s'));
        }

        /**
         * Returns the default opt-in confirmation message
         *
         * @return string
         *
         * @since 1.0.2
         */
        function get_default_optin_confirmation_message() {
            $default_optin_confirmation_message = esc_html__('Congratulations!!

Your email has been verified and we have sent the unlock link in your email.

Please check your inbox for the unlock link.

Thank you', 'subscribe-to-unlock');
            return $default_optin_confirmation_message;
        }

        /**
         * Returns the default confirmation email message
         *
         * @return string
         *
         * @since 1.0.2
         */
        function get_default_confirmation_email_message() {
            $default_email_message = esc_html__(sprintf('Hello There,

Thank you for subscribing in our %s website. Please confirm your subscription from below below link:

#confirmation_link

You will receive the unlock link as soon as you will confirm the subscription.

Thank you', esc_attr(get_bloginfo('name'))), 'subscribe-to-unlock');
            return $default_email_message;
        }

        /**
         * Generates current page URL
         *
         * @return string $pageURL
         *
         * @since 1.0.3
         */
        function get_current_page_url() {
            $pageURL = 'http';
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            $pageURL .= "://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
            $pageURL = explode('?', $pageURL);
            $pageURL = $pageURL[0];
            return $pageURL;
        }

        /**
         * Returns default message when unsubscribed
         *
         * @return string
         *
         * @since 1.0.3
         */
        function default_unsubscribe_message() {
            return esc_html__('You have unsubscribed successfully.', 'subscribe-to-unlock');
        }

        /**
         * Check if unlock key is valid or not
         *
         * @param string $unsubscribe_key
         * @return boolean
         *
         * @since 1.0.3
         */
        function is_valid_unlock_key($subscriber_unlock_key) {

            global $wpdb;
            $subscriber_table = STU_SUBSCRIBERS_TABLE;
            $subscribe_row_count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $subscriber_table WHERE subscriber_unlock_key like '%s'", $subscriber_unlock_key));
            if ($subscribe_row_count >= 1) {
                return true;
            } else {
                return false;
            }
        }

    }

    $GLOBALS['stu_library'] = new STU_Library();
}
