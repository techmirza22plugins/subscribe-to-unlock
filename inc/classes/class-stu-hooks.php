<?php

defined('ABSPATH') or die('No script kiddies please!!');
if (!class_exists('STU_Hooks')) {

    class STU_Hooks extends STU_Library {

        function __construct() {
            add_action('template_redirect', array($this, 'form_preview'));
            add_action('template_redirect', array($this, 'verify_link'));
            add_action('stu_end_form_process', array($this, 'process_third_party_subscriptions'), 10, 2);
            add_action('template_redirect', array($this, 'verify_subscription_confirmation'));
            add_action('template_redirect', array($this, 'unsubscribe_action'));
        }

        function form_preview() {
            if (isset($_GET['stu_preview'], $_GET['_wpnonce']) && $_GET['stu_preview'] && wp_verify_nonce($_GET['_wpnonce'], 'stu_form_preview_nonce') && is_user_logged_in()) {
                wp_enqueue_style('stu-preview', STU_URL . 'css/stu-preview.css', array(), STU_VERSION);
                include(STU_PATH . 'inc/views/frontend/form-preview.php');
                die();
            }
        }

        function process_third_party_subscriptions($form_data, $form_details) {
            if (!empty($form_data['stu_email'])) {
                if (!empty($form_data['stu_name'])) {
                    $name_array = explode(' ', $form_data['stu_name']);
                    $fname = $name_array[0];
                    $lname = (count($name_array) > 1) ? end($name_array) : '';
                } else {
                    $fname = '';
                    $lname = '';
                }
                if (!empty($form_details['general']['mailchimp_id'])) {
                    $post_parameters = array('members' => array(array('email_address' => $form_data['stu_email'], 'status' => 'subscribed', 'merge_fields' => array('FNAME' => $fname, 'LNAME' => $lname))), 'update_existing' => true);
                    /**
                     * Filters parameters being sent to Maichimp
                     *
                     * @param array $post_parameters
                     * @param array $form_data
                     * @param array $form_details
                     */
                    $post_parameters = apply_filters('stu_mc_post_parameters', $post_parameters, $form_data, $form_details);
                    foreach ($form_details['general']['mailchimp_id'] as $mailchimp_id) {
                        $this->subscribe_to_mailchimp($mailchimp_id, $post_parameters);
                    }
                }
                if (!empty($form_details['general']['constant_contact_id'])) {
                    $post_parameters = array(
                        'confirmed' => true,
                        'email_addresses' =>
                        array(
                            array(
                                'email_address' => $form_data['stu_email'],
                                'confirm_status' => 'NO_CONFIRMATION_REQUIRED'
                            ),
                        ),
                        'first_name' => $fname,
                        'last_name' => $lname,
                    );


                    /**
                     * stu_cc_post_parameters filters parameters being sent to Constant Contact
                     *
                     * @param array $post_parameters
                     * @param array $form_data
                     * @param array $form_details
                     */
                    $post_parameters = apply_filters('stu_cc_post_parameters', $post_parameters, $form_data, $form_details);
                    foreach ($form_details['general']['constant_contact_id'] as $constant_contact_id) {
                        $post_parameters['lists'][] = array('id' => $constant_contact_id);
                    }
                    $cc_connection = $this->subscribe_to_cc($constant_contact_id, $post_parameters);
                }
            }
        }

        function verify_link() {
            if (isset($_GET['stu_unlock_key'], $_COOKIE['stu_unlock_key']) && $_COOKIE['stu_unlock_key'] == $_GET['stu_unlock_key']) {
                $unlock_key = sanitize_text_field($_GET['stu_unlock_key']);
                $unlock_link_message = $this->get_unlock_link_message($unlock_key);
                setcookie('stu_unlock_check', 'yes', time() + (86400 * 30 * 365), "/");
                $form_alias = sanitize_text_field($_COOKIE['stu_alias']);

                $form_row = $this->get_form_row_by_alias($form_alias);
                $form_details = maybe_unserialize($form_row->form_details);
                $after_verification = (!empty($form_details['general']['after_verification'])) ? $form_details['general']['after_verification'] : 'show_unlock_message';
                if ($after_verification == 'redirect_form_page' && !empty($_COOKIE['stu_redirect_url'])) {
                    $this->change_verification_status($unlock_key);
                    wp_redirect($_COOKIE['stu_redirect_url']);
                    exit;
                } else {
                    echo $this->sanitize_html($unlock_link_message);
                    $this->change_verification_status($unlock_key);
                    die();
                }
            }
        }

        function verify_subscription_confirmation() {
            if (isset($_GET['stu_subscription_confirmation'], $_GET['confirmation_verification_key'])) {
                $confirmation_verification_key = sanitize_text_field($_GET['confirmation_verification_key']);
                if (!empty($_COOKIE['stu_email']) && !empty($_COOKIE['stu_alias'])) {
                    $subscriber_email = sanitize_email($_COOKIE['stu_email']);
                    $subscriber_name = (!empty($_COOKIE['stu_name'])) ? sanitize_text_field($_COOKIE['stu_name']) : '';
                    if (md5($subscriber_email) == $confirmation_verification_key) {
                        $form_alias = sanitize_text_field($_COOKIE['stu_alias']);
                        $form_row = $this->get_form_row_by_alias($form_alias);
                        $form_details = maybe_unserialize($form_row->form_details);
                        $form_data['stu_email'] = $subscriber_email;
                        $form_data['stu_name'] = $subscriber_name;
                        include(STU_PATH . '/inc/cores/unlock-email.php');
                        echo $this->sanitize_html($form_details['general']['optin_confirmation_message']);
                        die();
                    }
                }
            }
        }

        function unsubscribe_action() {
            if (!empty($_GET['stu_action']) && $_GET['stu_action'] == 'unsubscribe' && !empty($_GET['unsubscribe_key'])) {
                $unsubscribe_key = sanitize_text_field($_GET['unsubscribe_key']);

                if ($this->is_valid_unlock_key($unsubscribe_key)) {
                    global $wpdb;
                    $wpdb->delete(STU_SUBSCRIBERS_TABLE, array('subscriber_unlock_key' => $unsubscribe_key), array('%s'));
                    $stu_settings = get_option('stu_settings');
                    $unsubscribe_message = (!empty($stu_settings['extra']['unsubscribe_message'])) ? $stu_settings['extra']['unsubscribe_message'] : $this->default_unsubscribe_message();
                    echo $this->sanitize_html($unsubscribe_message);
                    die();
                }
            }
        }

    }

    new STU_Hooks();
}
