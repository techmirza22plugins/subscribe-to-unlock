<?php
defined('ABSPATH') or die('No script kiddies please!!');
if (!class_exists('STU_Admin')) {

    class STU_Admin extends STU_Library {

        function __construct() {
            add_action('admin_menu', array($this, 'add_stu_menu'));
            /**
             * Subscribers export to csv
             */
            add_action('admin_post_stu_export_csv', array($this, 'export_to_csv'));

            /**
             * Admin footer
             */
            add_action('admin_footer', array($this, 'add_extra_html'));
        }

        function add_stu_menu() {
            add_menu_page(esc_html__('Subscribe to Unlock', 'subscribe-to-unlock'), esc_html__('Subscribe to &nbsp; Unlock', 'subscribe-to-unlock'), 'manage_options', 'subscribe-to-unlock', array($this, 'generate_main_page'), 'dashicons-unlock');
            add_submenu_page('subscribe-to-unlock', esc_html__('Subscription Forms', 'subscribe-to-unlock'), esc_html__('Subscription Forms', 'subscribe-to-unlock'), 'manage_options', 'subscribe-to-unlock', array($this, 'generate_main_page'));
            add_submenu_page('subscribe-to-unlock', esc_html__('Add Subscription Form', 'subscribe-to-unlock'), esc_html__('Add Subscription Form', 'subscribe-to-unlock'), 'manage_options', 'stu-add-subscription-form', array($this, 'add_subscription_form'));
            add_submenu_page('subscribe-to-unlock', esc_html__('Subscribers', 'subscribe-to-unlock'), esc_html__('Subscribers', 'subscribe-to-unlock'), 'manage_options', 'stu-subscribers', array($this, 'generate_subscribers_list'));
            add_submenu_page('subscribe-to-unlock', esc_html__('Settings', 'subscribe-to-unlock'), esc_html__('Settings', 'subscribe-to-unlock'), 'manage_options', 'stu-settings', array($this, 'settings_page'));
            add_submenu_page('subscribe-to-unlock', esc_html__('Help', 'subscribe-to-unlock'), esc_html__('Help', 'subscribe-to-unlock'), 'manage_options', 'stu-help', array($this, 'render_help_page'));
            add_submenu_page('subscribe-to-unlock', esc_html__('About', 'subscribe-to-unlock'), esc_html__('About', 'subscribe-to-unlock'), 'manage_options', 'stu-about', array($this, 'render_about_page'));
        }

        function generate_main_page() {
            if (isset($_GET['action'], $_GET['form_id']) && $_GET['action'] == 'edit_form') {
                if (!empty($_GET['form_id'])) {
                    $form_id = intval($_GET['form_id']);
                    $form_row = $this->get_form_row_by_id($form_id);

                    if (empty($form_row)) {
                        echo sprintf(esc_html__("No form found with ID %d", 'subscribe-to-unlock'), $form_id);
                    } else {
                        $form_details = maybe_unserialize($form_row->form_details);
                    }
                }
                include(STU_PATH . 'inc/views/backend/forms/subscription-form-edit.php');
            } else {
                include(STU_PATH . 'inc/views/backend/forms/subscription-forms-list.php');
            }
        }

        function add_subscription_form() {
            include(STU_PATH . 'inc/views/backend/forms/subscription-form-add.php');
        }

        function generate_subscribers_list() {
            include(STU_PATH . 'inc/views/backend/subscribers/list-subscribers.php');
        }

        function export_to_csv() {
            if (!empty($_GET['_wpnonce']) && wp_verify_nonce($_GET['_wpnonce'], 'stu_export_csv_nonce')) {
                $filename = "subscribers-list.csv";
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename=' . $filename);
                $csv_rows = $this->get_subscriber_csv_rows();

                /**
                 * Filters csv rows
                 *
                 * @param array $csv_rows
                 *
                 * @since 1.0.0
                 */
                $csv_rows = apply_filters('stu_csv_rows', $csv_rows);

                $csv_output = fopen('php://output', 'w');
                foreach ($csv_rows as $csv_row) {
                    fputcsv($csv_output, $csv_row);
                }

                exit;
            } else {
                $this->permission_denied();
            }
        }

        function get_subscriber_csv_rows() {
            global $wpdb;
            $subscriber_table = STU_SUBSCRIBERS_TABLE;
            $form_table = STU_FORM_TABLE;
            if (!empty($_GET['form_alias'])) {

                $form_alias = sanitize_text_field($_GET['form_alias']);
                $subscriber_query = $wpdb->prepare("select * from $subscriber_table inner join $form_table on $subscriber_table.subscriber_form_alias = $form_table.form_alias where subscriber_form_alias = %s", $form_alias);
            } else {
                $subscriber_query = "select * from $subscriber_table inner join $form_table on $subscriber_table.subscriber_form_alias = $form_table.form_alias";
            }
            $subscriber_rows = $wpdb->get_results($subscriber_query);
            $csv_rows = array();
            $csv_rows[] = array(esc_html__('Subscriber Name', 'subscribe-to-unlock'), esc_html__('Subscriber Email', 'subscribe-to-unlock'), esc_html__('Subscriber Country', 'subscribe-to-unlock'), esc_html__('Subscriber Herd Size', 'subscribe-to-unlock'), esc_html__('Subscription Form', 'subscribe-to-unlock'), esc_html__('Verified', 'subscribe-to-unlock'));
            if (!empty($subscriber_rows)) {
                foreach ($subscriber_rows as $subscriber_row) {
                    $verification_status = (!empty($subscriber_row->subscriber_verification_status)) ? esc_html__('Yes', 'subscribe-to-unlock') : esc_html__('No', 'subscribe-to-unlock');
                    $csv_row = array($subscriber_row->subscriber_name, $subscriber_row->subscriber_email, $subscriber_row->subscriber_country, $subscriber_row->subscriber_farm_size, $subscriber_row->form_title, $verification_status);
                    $csv_rows[] = $csv_row;
                }
            }
            return $csv_rows;
        }

        function settings_page() {
            include(STU_PATH . 'inc/views/backend/stu-settings.php');
        }

        function render_help_page() {
            include(STU_PATH . 'inc/views/backend/stu-help.php');
        }

        function render_about_page() {
            include(STU_PATH . 'inc/views/backend/stu-about.php');
        }

        function add_extra_html() {
            ?>
            <script type="text/html" id="tmpl-mc-lists">
            <?php include(STU_PATH . 'inc/views/backend/js-templates/mailchimp-lists.php'); ?>
            </script>
            <script type="text/html" id="tmpl-email-domain">
            <?php include(STU_PATH . 'inc/views/backend/js-templates/email-domain-template.php'); ?>
            </script>
            <script type="text/html" id="tmpl-blacklisted-email">
                <?php include(STU_PATH . 'inc/views/backend/js-templates/blacklisted-email-template.php'); ?>
            </script>
            <?php
        }

    }

    new STU_Admin();
}