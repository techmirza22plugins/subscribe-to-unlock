<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!!' );
if ( !class_exists( 'STU_Enqueue' ) ) {

    class STU_Enqueue {

        function __construct() {
            add_action( 'wp_enqueue_scripts', array( $this, 'register_frontend_assets' ) );
            add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_assets' ) );
        }

        function register_frontend_assets() {
            $stu_frontend_obj = array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'ajax_nonce' => wp_create_nonce( 'stu_frontend_ajax_nonce' ) );
            $stu_settings = get_option( 'stu_settings' );
            if ( empty( $stu_settings['extra']['disable_fontawesome'] ) ) {
                wp_enqueue_style( 'fontawesome', STU_URL . 'fontawesome/css/all.min.css', array(), STU_VERSION );
            }

            wp_enqueue_script( 'stu-frontend-script', STU_JS_DIR . '/stu-frontend.js', array( 'jquery' ), STU_VERSION );
            wp_enqueue_style( 'stu-frontend-style', STU_CSS_DIR . '/stu-frontend.css', array(), STU_VERSION );
            if ( is_rtl() ) {
                wp_enqueue_style( 'stu-frontend-rtl-style', STU_CSS_DIR . '/stu-rtl.css', array(), STU_VERSION );
            }
            wp_localize_script( 'stu-frontend-script', 'stu_frontend_obj', $stu_frontend_obj );
            wp_enqueue_style( 'poppins', STU_URL . '/font-face/stylesheet.css', array(), STU_VERSION );
        }

        function register_admin_assets() {
            $translation_strings = array(
                'ajax_message' => esc_html__( 'Please wait', 'subscribe-to-unlock' ),
                'upload_button_text' => esc_html__( 'Upload File', 'subscribe-to-unlock' ),
                'delete_form_confirm' => esc_html__( 'Are you sure you want to delete this form?', 'subscribe-to-unlock' ),
                'copy_form_confirm' => esc_html__( 'Are you sure you want to copy this form?', 'subscribe-to-unlock' ),
                'clipboad_copy_message' => esc_html__( 'Shortcode copied to clipboard.', 'subscribe-to-unlock' ),
                'unsubscribe_clipboad_copy_message' => esc_html__( 'Unsubscribe link copied to clipboard.', 'subscribe-to-unlock' ),
                'invalid_api_key' => esc_html__( 'Invalid API Key.', 'subscribe-to-unlock' ),
                'mc_connect' => esc_html__( 'Connected', 'subscribe-to-unlock' ),
                'mc_disconnect' => esc_html__( 'Disconnected', 'subscribe-to-unlock' ),
                'mc_reset' => esc_html__( 'Are you sure you want to reset the Mailchimp connection?', 'subscribe-to-unlock' ),
                'cc_reset' => esc_html__( 'Are you sure you want to reset the Constant Contact connection?', 'subscribe-to-unlock' ),
                'delete_subscriber_confirm' => esc_html__( 'Are you sure you want to delete this subscriber?', 'subscribe-to-unlock' ),
            );
            $stu_backend_obj = array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'plugin_url' => STU_URL, 'translation_strings' => $translation_strings, 'ajax_nonce' => wp_create_nonce( 'stu_ajax_nonce' ) );
            wp_enqueue_media();
            wp_enqueue_style( 'wp-color-picker' );
            wp_enqueue_style( 'fontawesome', STU_URL . 'fontawesome/css/all.min.css', array(), STU_VERSION );
            wp_enqueue_style( 'stu-backend-style', STU_CSS_DIR . '/stu-backend.css', array(), STU_VERSION );
            wp_enqueue_style( 'stu-tinymce-style', STU_CSS_DIR . '/stu-tinymce.css', array(), STU_VERSION );
            wp_enqueue_script( 'stu-backend-script', STU_JS_DIR . '/stu-backend.js', array( 'jquery', 'wp-color-picker', 'wp-util' ), STU_VERSION );
            wp_localize_script( 'stu-backend-script', 'stu_backend_obj', $stu_backend_obj );
        }

    }

    new STU_Enqueue();
}