<?php

defined('ABSPATH') or die('No script kiddies please!');
if (!class_exists('STU_Tinymce')) {

    class STU_Tinymce {

        function __construct() {
            add_action('init', array($this, 'setup_tinymce_plugin'));
            add_action('admin_footer', array($this, 'tinymce_content_generator'));
        }

        function setup_tinymce_plugin() {

            // Check if the logged in WordPress User can edit Posts or Pages
            // If not, don't register our TinyMCE plugin

            if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
                return;
            }

            // Check if the logged in WordPress User has the Visual Editor enabled
            // If not, don't register our TinyMCE plugin
            if (get_user_option('rich_editing') !== 'true') {
                return;
            }

            // Setup some filters
            add_filter('mce_external_plugins', array(&$this, 'add_tinymce_plugin'));
            add_filter('mce_buttons', array(&$this, 'add_tinymce_toolbar_button'));
        }

        function add_tinymce_plugin($plugin_array) {
            $plugin_array['stu_shortcode_generator'] = STU_URL . 'js/stu-tinymce.js';
            return $plugin_array;
        }

        function add_tinymce_toolbar_button($buttons) {
            array_push($buttons, '', 'stu_shortcode_generator');
            return $buttons;
        }

        function tinymce_content_generator() {
            include(STU_PATH . 'inc/views/backend/tinymce-shortcode-popup.php');
        }

    }

    new STU_Tinymce();
}