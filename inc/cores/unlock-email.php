<?php

//It is okay to process the form now
$subscriber_row = $this->get_subscriber_row_by_email($form_data['stu_email']);
$from_email = (!empty($form_details['email']['from_email'])) ? $form_details['email']['from_email'] : $this->get_default_from_email();
$from_name = (!empty($form_details['email']['from_name'])) ? $form_details['email']['from_name'] : esc_html__('No Reply', 'subscribe-to-unlock');
$subject = (!empty($form_details['email']['subject'])) ? $form_details['email']['subject'] : esc_html__('Subscription Successful', 'subscribe-to-unlock');
$email_message = (!empty($form_details['email']['email_message'])) ? $form_details['email']['email_message'] : $this->get_default_email_message();

$charset = get_option('blog_charset');
$headers[] = 'Content-Type: text/html; charset=' . $charset;
$headers[] = "From: $from_name <$from_email>";
if (!empty($form_details['general']['dont_remember_email'])) {
    $subscriber_row = '';
}
if (empty($subscriber_row)) {
    //if subscriber isn't already subscribed
    $unlock_key = $this->generate_unlock_key();
    global $wpdb;
    $wpdb->insert(STU_SUBSCRIBERS_TABLE, array('subscriber_name' => $subscriber_name,
        'subscriber_email' => $form_data['stu_email'],
        'subscriber_country' => $form_data['stu_country'],
        'subscriber_farm_size' => $form_data['stu_farm_size'],
        'subscriber_form_alias' => $form_alias,
        'subscriber_unlock_key' => $unlock_key), array('%s', '%s', '%s', '%s'));



    if (!empty($form_details['general']['verification'])) {
        switch ($form_details['general']['verification_type']) {
            case 'link':
                $unlock_link = site_url() . '?stu_unlock_key=' . $unlock_key;
                $email_message = str_replace('#unlock_link', '<a href="' . $unlock_link . '">' . $unlock_link . '</a>', $email_message);
                break;
            case 'unlock_code':
                $email_message = str_replace('#unlock_code', $unlock_key, $email_message);
                break;
        }
        $email_check = wp_mail($form_data['stu_email'], $subject, $email_message, $headers);
        if ($email_check) {
            $unlock_code = $unlock_key;
            setcookie("stu_unlock_key", $unlock_key, time() + 3600 * 24 * 365, '/');
            setcookie("stu_alias", $form_alias, time() + 24 * 365, '/');
            $response['status'] = 200;
            $response['message'] = esc_attr($form_details['general']['success_message']);
            $response['verification_type'] = $form_details['general']['verification_type'];
            $response['unlock_key'] = $unlock_code;
        } else {
            $response['status'] = 403;
            $response['message'] = esc_attr($form_details['general']['error_message']);
        }
    } else {
        setcookie("stu_unlock_key", $unlock_key, time() + 3600 * 24 * 365, '/');
        setcookie('stu_unlock_check', 'yes', time() + (86400 * 30 * 365), "/");
        $response['status'] = 200;
        $response['message'] = esc_attr($form_details['general']['success_message']);
        $response['verification_type'] = 'none';
        $response['unlock_key'] = $unlock_key;
    }
} else {
    $unlock_key = $subscriber_row->subscriber_unlock_key;
    //if subscriber is already subscribed and verified
    if ($subscriber_row->subscriber_verification_status == 1) {

        setcookie("stu_unlock_key", $unlock_key, time() + 3600 * 24 * 365, '/');
        setcookie('stu_unlock_check', 'yes', time() + (86400 * 30 * 365), "/");
        $response['status'] = 200;
        $response['message'] = esc_attr($form_details['general']['success_message']);
        $response['verification_type'] = 'none';
        $response['unlock_key'] = $unlock_key;
    } else {
        if (!empty($form_details['general']['verification'])) {
            switch ($form_details['general']['verification_type']) {
                case 'link':
                    $unlock_link = site_url() . '?stu_unlock_key=' . $unlock_key;
                    $email_message = str_replace('#unlock_link', '<a href="' . $unlock_link . '">' . $unlock_link . '</a>', $email_message);
                    break;
                case 'unlock_code':
                    $email_message = str_replace('#unlock_code', $unlock_key, $email_message);
                    break;
            }
            $email_check = wp_mail($form_data['stu_email'], $subject, $email_message, $headers);
            if ($email_check) {
                $unlock_code = $unlock_key;
                setcookie("stu_unlock_key", $unlock_key, time() + 3600 * 24 * 365, '/');
                setcookie("stu_alias", $form_alias, time() + 24 * 365, '/');
                $response['status'] = 200;
                $response['message'] = esc_attr($form_details['general']['success_message']);
                $response['verification_type'] = $form_details['general']['verification_type'];
                $response['unlock_key'] = $unlock_code;
            } else {
                $response['status'] = 403;
                $response['message'] = esc_attr($form_details['general']['error_message']);
            }
        } else {
            setcookie("stu_unlock_key", $unlock_key, time() + 3600 * 24 * 365, '/');
            setcookie('stu_unlock_check', 'yes', time() + (86400 * 30 * 365), "/");
            $response['status'] = 200;
            $response['message'] = esc_attr($form_details['general']['success_message']);
            $response['verification_type'] = 'none';
            $response['unlock_key'] = $unlock_key;
        }
    }
}
/**
 * Triggers at the end of processing the subscription form successfully
 *
 * @param array $form_data
 *
 * @since 1.0.0
 */
do_action('stu_end_form_process', $form_data, $form_details);

