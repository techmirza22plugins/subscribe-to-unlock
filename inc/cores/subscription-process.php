<?php

defined('ABSPATH') or die('No script kiddies please!!');

if ($this->ajax_nonce_verify()) {
    /**
     * Triggers just before processing the subscription form
     *
     * @since 1.0.0
     */
    do_action('stu_before_form_process');
    $form_data = $_POST['form_data'];
    parse_str($form_data, $form_data);
    $form_data = $this->sanitize_array($form_data);
    $form_alias = sanitize_text_field($_POST['form_alias']);
    $form_row = $this->get_form_row_by_alias($form_alias);
    $form_details = maybe_unserialize($form_row->form_details);
    $subscriber_name = (!empty($form_data['stu_name'])) ? $form_data['stu_name'] : '';
    $subscriber_email = sanitize_email($form_data['stu_email']);
    if (empty($form_data['stu_email']) || 
    (!empty($form_details['form']['terms_agreement']['show']) && empty($form_data['stu_terms_agreement'])) || 
    (!empty($form_details['form']['name']['show']) && !empty($form_details['form']['name']['required']) && empty($form_data['stu_name'])) ||
    (!empty($form_details['form']['country']['required']) && empty($form_data['stu_country'])) ||
    (!empty($form_details['form']['farm_size']['required']) && empty($form_data['stu_farm_size']))
    ) {
        $response['status'] = 403;
        $response['message'] = esc_attr($form_details['general']['required_error_message']);
    } else {
        if (!empty($form_data['stu_redirect_url'])) {
            setcookie("stu_redirect_url", esc_url($form_data['stu_redirect_url']), time() + 24 * 365, '/');
        }
        if (!empty($form_details['general']['unallowed_email_domains'])) {

            $subscriber_email_array = explode('@', $subscriber_email);
            $email_domain = end($subscriber_email_array);
            if (in_array($email_domain, $form_details['general']['unallowed_email_domains'])) {
                $response['status'] = 403;
                $response['message'] = (!empty($form_details['general']['unallowed_email_error_message'])) ? esc_html($form_details['general']['unallowed_email_error_message']) : esc_html__('Invalid email domain', 'subscribe-to-unlock');
                die(json_encode($response));
            }
        }
        if (!empty($form_details['general']['blacklisted_emails'])) {

            if (in_array($subscriber_email, $form_details['general']['blacklisted_emails'])) {
                $response['status'] = 403;
                $response['message'] = (!empty($form_details['general']['unallowed_email_error_message'])) ? esc_html($form_details['general']['unallowed_email_error_message']) : esc_html__('Invalid email domain', 'subscribe-to-unlock');
                die(json_encode($response));
            }
        }

        if (!empty($form_details['general']['verification'])) {
            $subscriber_row = $this->get_subscriber_row_by_email($form_data['stu_email'], true);
        } else {
            $subscriber_row = $this->get_subscriber_row_by_email($form_data['stu_email']);
        }

        if (!empty($form_details['general']['dont_remember_email'])) {
            $subscriber_row = '';
        }
        /**
         * We are checking this if user has already subscribed so that the verification process won't get repeated
         */
        if (!empty($subscriber_row)) {
            // If user has already subscribed then we are skipping the verification process

            $unlock_key = $subscriber_row->subscriber_unlock_key;
            setcookie("stu_unlock_key", $unlock_key, time() + 3600 * 24 * 365, '/');
            setcookie('stu_unlock_check', 'yes', time() + (86400 * 30 * 365), "/");
            $response['status'] = 200;
            $response['message'] = esc_attr($form_details['general']['success_message']);
            $response['verification_type'] = 'none';
            $response['unlock_key'] = $unlock_key;
            die(json_encode($response));
        }
        if (!empty($form_details['general']['double_optin']) && $form_details['general']['verification_type'] == 'link') {
            setcookie("stu_name", $subscriber_name, time() + 24 * 365, '/');
            setcookie("stu_email", $subscriber_email, time() + 24 * 365, '/');
            setcookie("stu_alias", $form_alias, time() + 24 * 365, '/');


            $from_email = (!empty($form_details['email']['from_email'])) ? $form_details['email']['from_email'] : $this->get_default_from_email();
            $from_name = (!empty($form_details['email']['from_name'])) ? $form_details['email']['from_name'] : esc_html__('No Reply', 'subscribe-to-unlock');
            $confirmation_email_subject = (!empty($form_details['email']['confirmation_email_subject'])) ? $form_details['email']['confirmation_email_subject'] : esc_html__('Subscription Confirmation', 'subscribe-to-unlock');
            $confirmation_email_message = (!empty($form_details['email']['confirmation_email_message'])) ? $form_details['email']['confirmation_email_message'] : $this->get_default_confirmation_email_message();
            $confirmation_verification_key = md5($subscriber_email);
            $confirmation_verification_link = site_url() . '/?stu_subscription_confirmation=true&confirmation_verification_key=' . $confirmation_verification_key;
            $confirmation_email_message = str_replace('#confirmation_link', '<a href="' . $confirmation_verification_link . '">' . $confirmation_verification_link . '</a>', $confirmation_email_message);
            $charset = get_option('blog_charset');
            $headers[] = 'Content-Type: text/html; charset=' . $charset;
            $headers[] = "From: $from_name <$from_email>";
            $email_check = wp_mail($subscriber_email, $confirmation_email_subject, $confirmation_email_message, $headers);
            if ($email_check) {
                $response['status'] = 200;
                $response['verification_type'] = esc_html($form_details['general']['verification_type']);
                $response['confirmation_verification_link'] = $confirmation_verification_link;
                $response['message'] = esc_html($form_details['general']['success_message']);
            } else {
                $response['status'] = 403;
                $response['message'] = esc_html($form_details['general']['error_message']);
            }
        } else {

            include(STU_PATH . '/inc/cores/unlock-email.php');
        }
    }

    die(json_encode($response));
} else {
    $this->permission_denied();
}
