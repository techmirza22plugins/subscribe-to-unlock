<?php

if (!empty($form_details['custom']['custom_style'])) {
    /**
     * Text color
     */
    if (!empty($form_details['custom']['text_color'])) {
        $text_color = esc_attr($form_details['custom']['text_color']);
        $text_color_css = ".$alias_class{color:$text_color !important;}";
        wp_add_inline_style('stu-frontend-custom', $text_color_css);
    }
    /**
     * Button Color
     */
    if (!empty($form_details['custom']['button_background_color'])) {
        $button_background_color = esc_attr($form_details['custom']['button_background_color']);
        $button_background_color_css = ".$alias_class.stu-$form_template .stu-form-submit{background-color:$button_background_color;}";
        wp_add_inline_style('stu-frontend-custom', $button_background_color_css);
    }
    /**
     * Button Text Color
     */
    if (!empty($form_details['custom']['button_text_color'])) {
        $button_text_color = esc_attr($form_details['custom']['button_text_color']);
        $button_text_color_css = ".$alias_class.stu-$form_template .stu-form-submit{color:$button_text_color;}";
        wp_add_inline_style('stu-frontend-custom', $button_text_color_css);
    }
    /**
     * Button Hover Background Color
     */
    if (!empty($form_details['custom']['button_hover_color'])) {
        $button_hover_color = esc_attr($form_details['custom']['button_hover_color']);
        $button_hover_color_css = ".$alias_class.stu-$form_template .stu-form-submit:hover{background-color:$button_hover_color;}";
        wp_add_inline_style('stu-frontend-custom', $button_hover_color_css);
    }
    /**
     * Button Hover Text Color
     */
    if (!empty($form_details['custom']['button_hover_text_color'])) {
        $button_hover_text_color = esc_attr($form_details['custom']['button_hover_text_color']);
        $button_hover_text_color_css = ".$alias_class.stu-$form_template .stu-form-submit:hover{color:$button_hover_text_color;}";
        wp_add_inline_style('stu-frontend-custom', $button_hover_text_color_css);
    }

    /**
     * Loader Color
     */
    if (!empty($form_details['custom']['loader_color'])) {
        $loader_color = esc_attr($form_details['custom']['loader_color']);
        $loader_color_css = ".$alias_class.stu-$form_template span.stu-form-loader-wraper .stu-form-loader{color:$loader_color;}";
        wp_add_inline_style('stu-frontend-custom', $loader_color_css);
    }

    /**
     * Background
     */
    if (!empty($form_details['custom']['background_type'])) {
        $background_type = $form_details['custom']['background_type'];
        switch ($background_type) {
            case 'color':
                $background_color = esc_attr($form_details['custom']['background_color']);
                $background_color_css = ".$alias_class.stu-$form_template{background-color:$background_color;}";
                wp_add_inline_style('stu-frontend-custom', $background_color_css);
                if ($form_template == 'template-19') {
                    $before_color_css = ".$alias_class.stu-$form_template .stu-subs-img-box:before{background-color:$background_color}";
                    $after_color_css = ".$alias_class.stu-$form_template .stu-subs-img-box:after{background-color:$background_color}";
                    wp_add_inline_style('stu-frontend-custom', $before_color_css);
                    wp_add_inline_style('stu-frontend-custom', $after_color_css);
                }

                break;
            case 'image':
                $background_image = esc_attr($form_details['custom']['background_image']);
                $background_image_css = ".$alias_class.stu-$form_template{background-image:url('$background_image');}";
                wp_add_inline_style('stu-frontend-custom', $background_image_css);
                if ($form_template == 'template-19') {
                    $before_color_css = ".$alias_class.stu-$form_template .stu-subs-img-box:before{display:none}";
                    $after_color_css = ".$alias_class.stu-$form_template .stu-subs-img-box:after{display:none}";
                    wp_add_inline_style('stu-frontend-custom', $before_color_css);
                    wp_add_inline_style('stu-frontend-custom', $after_color_css);
                }
                break;
        }
    }
}


if (!empty($form_details['custom']['custom_css'])) {
    $custom_css = $this->sanitize_html($form_details['custom']['custom_css']);
    wp_add_inline_style('stu-frontend-custom', $custom_css);
}
global $stu_mobile_detector;
if (!empty($form_details['layout']['form_width']) && !$stu_mobile_detector->isMobile() && !$stu_mobile_detector->isTablet()) {
    $form_width = esc_attr($form_details['layout']['form_width']);
    $form_width_css = ".$alias_class{max-width:$form_width !important;}";
    wp_add_inline_style('stu-frontend-custom', $form_width_css);
}