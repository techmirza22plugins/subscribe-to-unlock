<?php
defined('ABSPATH') or die('No script kiddies please!!');
$stu_metabox_details = get_post_meta($post->ID, '_stu_metabox_details', true);
wp_nonce_field('stu_metabox_nonce', 'stu_metabox_nonce_field');
?>
<div class="stu-metabox-wrap">
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Enable', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="stu_metabox_details[enable_popup]" <?php echo (!empty($stu_metabox_details['enable_popup'])) ? 'checked="checked"' : ''; ?> value="1" class="stu-checkbox-toggle-trigger" data-toggle-class="stu-popup-enable-ref"/>
        </div>
    </div>
    <div class="stu-popup-enable-ref <?php echo (empty($stu_metabox_details['enable_popup'])) ? 'stu-display-none' : ''; ?>">
        <div class="stu-field-wrap stu-elem-block">
            <label><?php esc_html_e('Subscription Form', 'subscribe-to-unlock') ?></label>
            <div class="stu-field">
                <select name="stu_metabox_details[form_alias]">
                    <option value=""><?php esc_html_e("Choose Form", 'subscribe-to-unlock'); ?></option>
                    <?php
                    $selected_form_alias = (!empty($stu_metabox_details['form_alias'])) ? $stu_metabox_details['form_alias'] : '';
                    global $wpdb;
                    $form_table = STU_FORM_TABLE;
                    $forms = $wpdb->get_results("select * from $form_table order by form_title asc");
                    if (!empty($forms)) {
                        foreach ($forms as $form) {
                            ?>
                            <option value="<?php echo esc_attr($form->form_alias); ?>" <?php selected($selected_form_alias, $form->form_alias); ?>><?php echo esc_attr($form->form_title); ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="stu-field-wrap stu-elem-block">
            <label><?php esc_html_e('Display Type', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <?php
                $display_type = (!empty($stu_metabox_details['display_type'])) ? $stu_metabox_details['display_type'] : 'countdown';
                ?>
                <select name="stu_metabox_details[display_type]" class="stu-toggle-trigger" data-toggle-class="stu-display-type-toggle">
                    <option value="direct" <?php selected($display_type, 'direct'); ?>><?php esc_html_e('Direct on Load', 'subscribe-to-unlock'); ?></option>
                    <option value="countdown"<?php selected($display_type, 'countdown'); ?>><?php esc_html_e('Countdown', 'subscribe-to-unlock'); ?></option>
                    <option value="delay" <?php selected($display_type, 'delay'); ?>><?php esc_html_e('Delay', 'subscribe-to-unlock'); ?></option>
                </select>
            </div>
        </div>
        <div class="stu-display-type-toggle stu-countdown-fields-ref <?php echo ($display_type != 'countdown') ? 'stu-display-none' : ''; ?>" data-toggle-ref="countdown">
            <div class="stu-field-wrap stu-elem-block">
                <label><?php esc_html_e('Countdown Timer', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <input type="number" name="stu_metabox_details[countdown_timer]" min="0" value="<?php echo (!empty($stu_metabox_details['countdown_timer'])) ? intval($stu_metabox_details['countdown_timer']) : ''; ?>"/>
                    <p class="description"><?php esc_html_e('Please enter the countdown timer for popup display in seconds. Please leave empty or enter 0 to disable countdown timer.', 'subscribe-to-unlock'); ?></p>
                </div>
            </div>
            <div class="stu-field-wrap stu-elem-block">
                <label><?php esc_html_e('Countdown Timer Message', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <textarea name="stu_metabox_details[countdown_timer_message]"><?php echo (!empty($stu_metabox_details['countdown_timer_message'])) ? esc_attr($stu_metabox_details['countdown_timer_message']) : ''; ?></textarea>

                </div>
                <p class="description"><?php esc_html_e('Please use #countdown to replace it with the actual countdown timer in the message.', 'subscribe-to-unlock'); ?></p>
                <p class="description"><?php esc_html_e('For eg: Please subscribe to unlock the page or wait #countdown seconds.', 'subscribe-to-unlock'); ?></p>
            </div>
        </div>
        <div class="stu-display-type-toggle stu-delay-fields-ref <?php echo ($display_type != 'delay') ? 'stu-display-none' : ''; ?>" data-toggle-ref="delay">
            <div class="stu-field-wrap stu-elem-block">
                <label><?php esc_html_e('Delay Time', 'subscribe-to-unlock'); ?></label>
                <div>
                    <input type="number" name="stu_metabox_details[delay_time]" min="0" value="<?php echo (!empty($stu_metabox_details['delay_time'])) ? intval($stu_metabox_details['delay_time']) : ''; ?>"/>
                    <p class="description"><?php esc_html_e('Please enter the delay time for popup to display in seconds. Please leave empty or enter 0 to disable delay', 'subscribe-to-unlock'); ?></p>
                </div>
            </div>
        </div>
    </div>

</div>