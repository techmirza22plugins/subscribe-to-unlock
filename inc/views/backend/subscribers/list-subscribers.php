<?php
defined('ABSPATH') or die('No script kiddies please!!');
?>
<div class="wrap stu-wrap">
    <div class="stu-header stu-clearfix">
        <h1 class="stu-floatLeft">
            <img src="<?php echo STU_URL . 'images/logo.png' ?>" class="stu-plugin-logo" />
            <span class="stu-sub-header"><?php esc_html_e('Subscribers', 'subscribe-to-unlock'); ?></span>
        </h1>

        <div class="stu-add-wrap">
            <?php
            if (!empty($_GET['form_alias'])) {
                $form_alias = sanitize_text_field($_GET['form_alias']);
                $export_url = admin_url('admin-post.php?action=stu_export_csv&form_alias=' . $form_alias . '&_wpnonce=' . wp_create_nonce('stu_export_csv_nonce'));
            } else {
                $export_url = admin_url('admin-post.php?action=stu_export_csv&_wpnonce=' . wp_create_nonce('stu_export_csv_nonce'));
            }
            ?>
            <a href="<?php echo esc_url($export_url); ?>"><input type="button" class="stu-button-orange" value="<?php esc_html_e('Export to CSV', 'subscribe-to-unlock'); ?>"></a>
        </div>
    </div>
    <div class="stu-form-wrap">
        <div class="stu-export-wrap">
            <form method="get" action="<?php echo admin_url('admin.php'); ?>" class="stu-subscriber-export-form">
                <input type="hidden" name="page" value="stu-subscribers"/>
                <select name="form_alias" class="stu-export-alias-trigger">
                    <option value=""><?php esc_html_e('All', 'subscribe-to-unlock'); ?></option>
                    <?php
                    global $wpdb;
                    $form_table = STU_FORM_TABLE;
                    $form_rows = $wpdb->get_results("select * from $form_table order by form_title asc");
                    $selected_form_alias = (!empty($_GET['form_alias'])) ? sanitize_text_field($_GET['form_alias']) : '';
                    if (!empty($form_rows)) {
                        foreach ($form_rows as $form_row) {
                            ?>
                            <option value="<?php echo esc_attr($form_row->form_alias); ?>" <?php selected($selected_form_alias, $form_row->form_alias); ?>><?php echo esc_attr($form_row->form_title); ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </form>

        </div>
        <table class="wp-list-table widefat fixed stu-form-lists-table">
            <thead>
                <tr>
                    <th><?php esc_html_e('Subscriber Name', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Subscriber Email', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Subscriber Country', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Subscriber Farm Size', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Form', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Verified', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Action', 'subscribe-to-unlock'); ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                global $wpdb;
                $subscriber_table = STU_SUBSCRIBERS_TABLE;
                $form_table = STU_FORM_TABLE;
                if (!empty($_GET['form_alias'])) {
                    $form_alias = sanitize_text_field($_GET['form_alias']);
                    $subscriber_query = $wpdb->prepare("select * from $subscriber_table inner join $form_table on $subscriber_table.subscriber_form_alias = $form_table.form_alias where subscriber_form_alias = %s", $form_alias);
                } else {
                    $subscriber_query = "select * from $subscriber_table inner join $form_table on $subscriber_table.subscriber_form_alias = $form_table.form_alias";
                }
                $subscriber_rows = $wpdb->get_results($subscriber_query);
                if (!empty($subscriber_rows)) {
                    foreach ($subscriber_rows as $subscriber_row) {
                        ?>
                        <tr>
                            <td><?php echo esc_attr($subscriber_row->subscriber_name); ?></td>
                            <td><?php echo esc_attr($subscriber_row->subscriber_email); ?></td>
                            <td><?php echo esc_attr($subscriber_row->subscriber_country); ?></td>
                            <td><?php echo esc_attr($subscriber_row->subscriber_farm_size); ?></td>
                            <td><?php echo esc_attr($subscriber_row->form_title); ?></td>
                            <td><?php echo (!empty($subscriber_row->subscriber_verification_status)) ? esc_html__('Yes', 'subscribe-to-unlock') : esc_html__('No', 'subscribe-to-unlock'); ?></td>
                            <td>
                                <a class="stu-delete stu-subscriber-delete" href="javascript:void(0)" data-subscriber-id="<?php echo intval($subscriber_row->subscriber_id); ?>" title="<?php esc_attr_e('Delete Subscriber', 'subscribe-to-unlock'); ?>"><?php esc_html_e('Delete', 'subscribe-to-unlock'); ?></a>

                            </td>
                            <td>
                                <a class="stu-unsubscribe-link" href="javascript:void(0)" title="<?php esc_attr_e('Click to copy', 'subscribe-to-unlock'); ?>"><input type="button" class="button-secondary" value="<?php esc_html_e('Copy unsubscribe link', 'subscribe-to-unlock'); ?>"/></a>
                                <span class="stu-unsubscribe-link-ref stu-display-none"><?php echo site_url() . '/?stu_action=unsubscribe&unsubscribe_key=' . $subscriber_row->subscriber_unlock_key; ?></span>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="6"><?php esc_html_e('No subscribers found.', 'subscribe-to-unlock'); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <th><?php esc_html_e('Subscriber Name', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Subscriber Email', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Subscriber Country', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Subscriber Farm Size', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Form', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Downaload Status', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Action', 'subscribe-to-unlock'); ?></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<div class="stu-form-message"></div>
