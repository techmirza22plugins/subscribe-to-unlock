<div class="stu-form-nav-wrap">
    <ul class="stu-form-nav">
        <li><a href="javascript:void(0);" class="stu-nav-item stu-active-nav" data-tab="general"><span class="dashicons dashicons-admin-generic"></span><?php esc_html_e( 'General Settings', 'subscribe-to-unlock' ); ?></a></li>
        <li><a href="javascript:void(0);" class="stu-nav-item" data-tab="form"><span class="dashicons dashicons-feedback"></span><?php esc_html_e( 'Form Settings', 'subscribe-to-unlock' ); ?></a></li>
        <li><a href="javascript:void(0);" class="stu-nav-item" data-tab="layout"><span class="dashicons dashicons-layout"></span><?php esc_html_e( 'Layout Settings', 'subscribe-to-unlock' ); ?></a></li>
        <li><a href="javascript:void(0);" class="stu-nav-item" data-tab="email"><span class="dashicons dashicons-email"></span><?php esc_html_e( 'Email Settings', 'subscribe-to-unlock' ); ?></a></li>
        <li><a href="javascript:void(0);" class="stu-nav-item" data-tab="custom"><span class="dashicons dashicons-admin-customizer"></span><?php esc_html_e( 'Custom', 'subscribe-to-unlock' ); ?></a></li>
    </ul>
</div>