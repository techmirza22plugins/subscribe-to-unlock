<div class="stu-settings-each-section stu-display-none" data-tab="email">


    <div class="stu-field-wrap">
        <label><?php esc_html_e('From Email', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="text" name="form_details[email][from_email]" value="<?php echo (!empty($form_details['email']['from_email'])) ? esc_attr($form_details['email']['from_email']) : ''; ?>"/>
            <p class="description"><?php esc_html_e('Please enter the from email which will be used to send the email to subscribers. Please enter any email which won\'t resembele the real person\'s email such as noreply@yoursiteurl.com ', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('From Name', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="text" name="form_details[email][from_name]" value="<?php echo (!empty($form_details['email']['from_name'])) ? esc_attr($form_details['email']['from_name']) : ''; ?>"/>
            <p class="description"><?php esc_html_e('Please enter the from name which will be used to send the email to subscribers. Please enter any email which won\'t resembele the real person\'s email such as No Reply ', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <div class="stu-verification-type-ref" <?php echo ($verification_type == 'unlock_code') ? 'style="display:none;"' : ''; ?> data-toggle-ref="link">
        <div class="stu-double-optin-ref" <?php echo (empty($form_details['general']['double_optin']) || $verification_type == 'unlock_code') ? 'style="display:none;"' : ''; ?> >
            <div class="stu-field-wrap">
                <label><?php esc_html_e('Confirmation Email Subject', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <input type="text" name="form_details[email][confirmation_email_subject]" value="<?php echo (!empty($form_details['email']['confirmation_email_subject'])) ? esc_attr($form_details['email']['confirmation_email_subject']) : ''; ?>"/>
                </div>
            </div>
            <div class="stu-field-wrap">
                <label><?php esc_html_e('Confirmation Email Message', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <textarea name="form_details[email][confirmation_email_message]"><?php echo (!empty($form_details['email']['confirmation_email_message'])) ? $this->output_converting_br($form_details['email']['confirmation_email_message']) : $this->get_default_confirmation_email_message(); ?></textarea>
                    <p class="description"><?php esc_html_e('Please use #confirmation_link which will be replaced with the confirmation link in the email.', 'subscribe-to-unlock'); ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Verification Email Subject', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="text" name="form_details[email][subject]" value="<?php echo (!empty($form_details['email']['subject'])) ? esc_attr($form_details['email']['subject']) : ''; ?>"/>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Verification Email Message', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <textarea name="form_details[email][email_message]"><?php echo (!empty($form_details['email']['email_message'])) ? $this->output_converting_br($form_details['email']['email_message']) : $this->get_default_email_message(); ?></textarea>
            <p class="description"><?php esc_html_e('Please use #unlock_link which will be replaced by unlock link in the email.', 'subscribe-to-unlock'); ?></p>
            <p class="description"><?php esc_html_e('Please use #unlock_code which will be replaced by unlock code in the email.', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
</div>