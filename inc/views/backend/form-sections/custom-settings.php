<div class="stu-settings-each-section  stu-display-none" data-tab="custom">
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Enable Custom Styles', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="form_details[custom][custom_style]" value="1" <?php echo (!empty($form_details['custom']['custom_style'])) ? 'checked="checked"' : ''; ?> class="stu-checkbox-toggle-trigger" data-toggle-class="stu-custom-style-enabled"/>
        </div>
    </div>
    <?php $custom_style_enable = (!empty($form_details['custom']['custom_style'])) ? 1 : 0; ?>
    <div class="stu-custom-style-enabled" <?php $this->display_none($custom_style_enable, 1); ?>>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Text Color', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input class="stu-color-picker" type="text" name="form_details[custom][text_color]" value="<?php echo (!empty($form_details['custom']['text_color'])) ? esc_attr($form_details['custom']['text_color']) : ''; ?>"/>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Button Background Color', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input class="stu-color-picker" type="text" name="form_details[custom][button_background_color]" value="<?php echo (!empty($form_details['custom']['button_background_color'])) ? esc_attr($form_details['custom']['button_background_color']) : ''; ?>"/>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Button Text Color', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input class="stu-color-picker" type="text" name="form_details[custom][button_text_color]" value="<?php echo (!empty($form_details['custom']['button_text_color'])) ? esc_attr($form_details['custom']['button_text_color']) : ''; ?>"/>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Button Hover Color', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input class="stu-color-picker" type="text" name="form_details[custom][button_hover_color]" value="<?php echo (!empty($form_details['custom']['button_hover_color'])) ? esc_attr($form_details['custom']['button_hover_color']) : ''; ?>"/>
            </div>
        </div>

        <div class="stu-field-wrap">
            <label><?php esc_html_e('Button Hover Text Color', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input class="stu-color-picker" type="text" name="form_details[custom][button_hover_text_color]" value="<?php echo (!empty($form_details['custom']['button_hover_text_color'])) ? esc_attr($form_details['custom']['button_hover_text_color']) : ''; ?>"/>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Loader Color', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input class="stu-color-picker" type="text" name="form_details[custom][loader_color]" value="<?php echo (!empty($form_details['custom']['loader_color'])) ? esc_attr($form_details['custom']['loader_color']) : ''; ?>"/>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Background Type', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <select name="form_details[custom][background_type]" data-toggle-class='stu-background-ref' class="stu-toggle-trigger">
                    <?php $selected_background_type = (!empty($form_details['custom']['background_type'])) ? $form_details['custom']['background_type'] : ''; ?>
                    <option value=""><?php esc_html_e('Default', 'subscribe-to-unlock'); ?></option>
                    <option value="color" <?php selected($selected_background_type, 'color'); ?>><?php esc_html_e('Color', 'subscribe-to-unlock'); ?></option>
                    <option value="image" <?php selected($selected_background_type, 'image'); ?>><?php esc_html_e('Image', 'subscribe-to-unlock'); ?></option>
                </select>
            </div>
        </div>
        <div class="stu-field-wrap stu-background-ref" data-toggle-ref='color' <?php $this->display_none($selected_background_type, 'color'); ?>>
            <label><?php esc_html_e('Background Color', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input type="text" name="form_details[custom][background_color]" class="stu-color-picker" value="<?php echo (!empty($form_details['custom']['background_color'])) ? esc_attr($form_details['custom']['background_color']) : ''; ?>"/>
            </div>
        </div>
        <div class="stu-background-ref" data-toggle-ref='image' <?php $this->display_none($selected_background_type, 'image'); ?>>
            <div class="stu-field-wrap">
                <label><?php esc_html_e('Background Image', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <input type="text" name="form_details[custom][background_image]" value="<?php echo (!empty($form_details['custom']['background_image'])) ? esc_url($form_details['custom']['background_image']) : ''; ?>"/>
                    <input type="hidden" name="form_details[custom][background_image_id]" value="<?php echo (!empty($form_details['custom']['background_image_id'])) ? intval($form_details['custom']['background_image_id']) : ''; ?>"/>
                    <input type="button" class="button-secondary stu-file-uploader" value="<?php esc_html_e('Upload Image', 'subscribe-to-unlock'); ?>"/>
                    <div class="stu-image-preview"><?php
                        if (!empty($form_details['custom']['background_image']) && !empty($form_details['custom']['background_image_id'])) {
                            $thumb_url = wp_get_attachment_thumb_url($form_details['custom']['background_image_id']);
                            ?>
                            <img src="<?php echo esc_url($thumb_url) ?>"/>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Custom CSS', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <textarea name="form_details[custom][custom_css]"><?php echo (!empty($form_details['custom']['custom_css'])) ? $form_details['custom']['custom_css'] : ''; ?></textarea>
        </div>
    </div>
</div>
