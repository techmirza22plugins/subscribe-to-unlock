<?php $selected_layout = (!empty($form_details['layout']['template'])) ? esc_attr($form_details['layout']['template']) : 'template-1'; ?>
<div class="stu-settings-each-section stu-display-none" data-tab="layout">

    <div class="stu-field-wrap">
        <label><?php esc_html_e('Form Layout', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <select name="form_details[layout][template]" class="stu-toggle-trigger" data-toggle-class="stu-form-layout">
                <?php
                for ($i = 1; $i <= STU_TOTAL_TEMPLATES; $i++) {
                    ?>
                    <option value="template-<?php echo intval($i); ?>" <?php echo selected($selected_layout, 'template-' . $i); ?>>Template <?php echo intval($i); ?></option>
                    <?php
                }
                ?>
            </select>
            <div class="stu-preview-img-wrapper">
                <?php
                for ($i = 1; $i <= STU_TOTAL_TEMPLATES; $i++) {
                    ?>
                    <div class="stu-form-layout" <?php $this->display_none($selected_layout, 'template-' . $i); ?> data-toggle-ref="template-<?php echo intval($i); ?>">
                        <img src="<?php echo esc_url(STU_IMG_DIR . '/template-previews/template-' . $i . '.jpg'); ?>"/>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Form Width', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="text" name="form_details[layout][form_width]" value="<?php echo (!empty($form_details['layout']['form_width'])) ? esc_attr($form_details['layout']['form_width']) : ''; ?>" placeholder="<?php esc_html_e('500px or 100%', 'subscribe-to-unlock'); ?>"/>
            <p class="description"><?php esc_html_e('Please enter the width of the form either in px or %', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
</div>