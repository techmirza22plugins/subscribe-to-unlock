<div class="stu-settings-each-section" data-tab="general">
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Form Status', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="form_status" value="1" <?php echo (!empty($form_row->form_status)) ? 'checked="checked"' : ''; ?>/>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Form Title', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="text" name="form_title" value="<?php echo (!empty($form_row->form_title)) ? esc_attr($form_row->form_title) : ''; ?>"/>
        </div>
    </div>

    <div class="stu-field-wrap">
        <label><?php esc_html_e('Form Alias', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="text" name="form_alias" class="stu-alias-field" value="<?php echo (!empty($form_row->form_alias)) ? esc_attr($form_row->form_alias) : ''; ?>" <?php echo (!empty($_GET['form_id'])) ? 'readonly="readonly"' : ''; ?>/>
            <?php if (!empty($_GET['form_id'])) {
                ?>
                <input type="button" class="button-secondary stu-alias-force-edit" value="<?php esc_html_e('Edit Anyway', 'subscribe-to-unlock'); ?>"/>
                <?php
            }
            ?>
            <p class="description">
                <?php
                if (!isset($_GET['form_id'])) {
                    esc_html_e('Alias should be unique and shouldn\'t contain any special characters and please use _ instead of space.', 'subscribe-to-unlock');
                } else {
                    esc_html_e('Alias cannot be modified once added because it is used as the reference for fetching the subscribers.', 'subscribe-to-unlock');
                }
                ?>
            </p>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Lock Content', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <?php
            $lock_content = (!empty($form_details['general']['lock_content'])) ? $this->sanitize_html($form_details['general']['lock_content']) : '';
            wp_editor($lock_content, 'stu_lock_content', array('textarea_name' => 'form_details[general][lock_content]'));
            ?>
            <p class="description"><?php esc_html_e('Please leave the lock content blank if you are willing to lock the content directly from your post or page editor. And you can also use shortcode from other plugin inside the lock content if required.', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Lock Mode', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <select name="form_details[general][lock_mode]">
                <?php
                $selected_lock_mode = (!empty($form_details['general']['lock_mode'])) ? $form_details['general']['lock_mode'] : '';
                ?>
                <option value="soft" <?php selected($selected_lock_mode, 'soft'); ?>><?php esc_html_e('Soft Lock', 'subscribe-to-unlock'); ?></option>
                <option value="hard" <?php selected($selected_lock_mode, 'hard'); ?>><?php esc_html_e('Hard Lock', 'subscribe-to-unlock'); ?></option>
            </select>
            <p class="description"><?php esc_html_e('Soft lock will blur the content whereas hard lock will completely hide the content.', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>

    <div class="stu-field-wrap">
        <label><?php esc_html_e('Verification', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="form_details[general][verification]" value="1" <?php echo (!empty($form_details['general']['verification'])) ? 'checked="checked"' : ''; ?> class="stu-checkbox-toggle-trigger" data-toggle-class="stu-verification-ref"/>
            <p class="description"><?php esc_html_e('Please check if you want to enable the email verification of the subscriber before unlocking the content.', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <?php
    $verification = (!empty($form_details['general']['verification'])) ? 1 : 0;
    $verification_type = (!empty($form_details['general']['verification_type'])) ? $form_details['general']['verification_type'] : 'link';
    ?>
    <div class="stu-verification-ref" <?php $this->display_none($verification, 1); ?>>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Verification Type', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <select name="form_details[general][verification_type]" class="stu-toggle-trigger" data-toggle-class="stu-verification-type-ref">
                    <option value="link" <?php selected($verification_type, 'link'); ?>><?php esc_html_e('Verification through link', 'subscribe-to-unlock'); ?></option>
                    <option value="unlock_code" <?php selected($verification_type, 'unlock_code'); ?>><?php esc_html_e('Verification through code', 'subscribe-to-unlock'); ?></option>
                </select>
                <p class="description"><?php esc_html_e('If verification through link is selected, the verification link will be sent in the email.', 'subscribe-to-unlock'); ?></p>
                <p class="description"><?php esc_html_e('If verification through code is selected, the verification code will be sent in the email which can be entered in the form for unlocking the content.', 'subscribe-to-unlock'); ?></p>
            </div>
        </div>
        <div class="stu-verification-type-ref" <?php $this->display_none($verification_type, 'unlock_code'); ?> data-toggle-ref="unlock_code">
            <div class="stu-field-wrap">
                <label><?php esc_html_e('Unlock Message', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <textarea name="form_details[general][unlock_message]"><?php echo (!empty($form_details['general']['unlock_message'])) ? $this->sanitize_html($form_details['general']['unlock_message']) : ''; ?></textarea>
                </div>
            </div>
            <div class="stu-field-wrap">
                <label><?php esc_html_e('Unlock Button Label', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <input type="text" name="form_details[general][unlock_button_label]" value="<?php echo (!empty($form_details['general']['unlock_button_label'])) ? esc_attr($form_details['general']['unlock_button_label']) : ''; ?>"/>
                </div>
            </div>
            <div class="stu-field-wrap">
                <label><?php esc_html_e('Unlock Error Message', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <textarea name="form_details[general][unlock_error_message]"><?php echo (!empty($form_details['general']['unlock_error_message'])) ? $this->sanitize_html($form_details['general']['unlock_error_message']) : ''; ?></textarea>
                </div>
            </div>
        </div>
        <div class="stu-verification-type-ref" <?php $this->display_none($verification_type, 'link'); ?> data-toggle-ref="link">
            <div class="stu-field-wrap">
                <label><?php esc_html_e('After Verification', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <?php
                    $after_verification = (!empty($form_details['general']['after_verification'])) ? $form_details['general']['after_verification'] : 'show_unlock_message';
                    ?>
                    <select name="form_details[general][after_verification]" class="stu-toggle-trigger" data-toggle-class='stu-unlock-link-message'>
                        <option value="show_unlock_message" <?php selected($after_verification, 'show_unlock_message'); ?>><?php esc_html_e('Show Unlock Message', 'subscribe-to-unlock'); ?></option>
                        <option value="redirect_form_page" <?php selected($after_verification, 'redirect_form_page'); ?>><?php esc_html_e('Redirect to form Page', 'subscribe-to-unlock'); ?></option>
                    </select>
                    <p class="description"><?php esc_html_e('If redirect to form page option is choosen, the user will be redirected to the URL from which the form has been submitted with the content unlocked.', 'subscribe-to-unlock'); ?></p>
                </div>
            </div>
            <div class="stu-field-wrap stu-unlock-link-message" data-toggle-ref='show_unlock_message' <?php echo $this->display_none($after_verification, 'show_unlock_message'); ?>>
                <label><?php esc_html_e('Unlock Link Message', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <textarea name="form_details[general][unlock_link_message]"><?php echo (!empty($form_details['general']['unlock_link_message'])) ? $this->output_converting_br($form_details['general']['unlock_link_message']) : $this->get_default_unlock_link_message(); ?></textarea>
                    <p class="description"><?php esc_html_e('Please enter the message to be displayed when the unlock link is clicked.', 'subscribe-to-unlock'); ?></p>
                </div>
            </div>
            <div class="stu-field-wrap">
                <label><?php esc_html_e('Double Opt-In', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <input type="checkbox" name="form_details[general][double_optin]" value="1" class="stu-checkbox-toggle-trigger" data-toggle-class="stu-double-optin-ref" <?php echo (!empty($form_details['general']['double_optin'])) ? 'checked="checked"' : ''; ?>/>
                    <p class="description"><?php esc_html_e('Please check this if you want to send the unlock link to subscriber only after clicking the subscription confirmation link which will be sent in the email as soon as the subscriber subscribe.', 'subscribe-to-unlock'); ?></p>
                </div>
            </div>
            <div class="stu-field-wrap stu-double-optin-ref" <?php echo (empty($form_details['general']['double_optin'])) ? 'style="display:none";' : ''; ?>>
                <label><?php esc_html_e('Opt-In Confirmation Message', 'subscribe-to-unlock'); ?></label>
                <div class="stu-field">
                    <textarea name="form_details[general][optin_confirmation_message]"><?php echo (!empty($form_details['general']['optin_confirmation_message'])) ? $this->output_converting_br($form_details['general']['optin_confirmation_message']) : $this->get_default_optin_confirmation_message(); ?></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Don\'t remember email', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="form_details[general][dont_remember_email]" value="1" <?php echo (!empty($form_details['general']['dont_remember_email'])) ? 'checked="checked"' : ''; ?>/>
            <p class="description"><?php esc_html_e('Please check if you don\'t want to check the email of the subscriber if already subscribed or not.', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Form Success Message', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <textarea name="form_details[general][success_message]"><?php echo (!empty($form_details['general']['success_message'])) ? esc_attr($form_details['general']['success_message']) : ''; ?></textarea>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Form Required Error Message', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <textarea name="form_details[general][required_error_message]"><?php echo (!empty($form_details['general']['required_error_message'])) ? esc_attr($form_details['general']['required_error_message']) : ''; ?></textarea>
            <p class="description"><?php esc_html_e('Please enter the message that needs to be shown when any validation error occurs in form submission.', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Form Error Message', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <textarea name="form_details[general][error_message]"><?php echo (!empty($form_details['general']['error_message'])) ? esc_attr($form_details['general']['error_message']) : ''; ?></textarea>
            <p class="description"><?php esc_html_e("Please enter the message that needs to be shown when email couldn't be sent.", 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Unallowed Email Domains', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <div class="stu-unallowed-email-domains-list">
                <?php
                if (!empty($form_details['general']['unallowed_email_domains'])) {
                    foreach ($form_details['general']['unallowed_email_domains'] as $email_domain) {
                        ?>
                        <div class="stu-each-domain">
                            <input type="text" name="form_details[general][unallowed_email_domains][]" value="<?php echo esc_attr($email_domain); ?>"/>
                            <span class="dashicons dashicons-trash stu-remove-domain-trigger"></span>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <input type="button" class="button-secondary stu-domain-add-trigger" value="<?php esc_attr_e('Add Domain', 'subscribe-to-unlock') ?>"/>

        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Blacklisted Emails', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">

            <div class="stu-blacklisted-email-list">
                <?php
                if (!empty($form_details['general']['blacklisted_emails'])) {
                    foreach ($form_details['general']['blacklisted_emails'] as $blacklisted_email) {
                        ?>
                        <div class="stu-each-blacklisted-email">
                            <input type="email" name="form_details[general][blacklisted_emails][]" value="<?php echo esc_attr($blacklisted_email); ?>"/>
                            <span class="dashicons dashicons-trash stu-remove-blacklisted-email-trigger"></span>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <input type="button" class="button-secondary stu-blacklisted-email-add-trigger" value="<?php esc_attr_e('Add Email', 'subscribe-to-unlock') ?>"/>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Unallowed Email Error Message', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="text" name="form_details[general][unallowed_email_error_message]" placeholder="<?php esc_html_e('Unallowed Email Domain', 'subscribe-to-unlock'); ?>" value="<?php echo (!empty($form_details['general']['unallowed_email_error_message'])) ? esc_attr($form_details['general']['unallowed_email_error_message']) : ''; ?>"/>
            <p class="description"><?php esc_html_e('Please enter the message to show when any unallowed email domain is entered.', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <?php
    $stu_settings = get_option('stu_settings');
    if (!empty($stu_settings['mailchimp']['enable']) && !empty($stu_settings['mailchimp']['status']) && !empty($stu_settings['mailchimp']['mailchimp_lists'])) {
        ?>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Mailchimp Lists', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <?php
                $mailchimp_lists = json_decode($stu_settings['mailchimp']['mailchimp_lists']);
                $selected_mailchimp_lists = (!empty($form_details['general']['mailchimp_id'])) ? $form_details['general']['mailchimp_id'] : array();
                if (!empty($mailchimp_lists->lists)) {
                    $mailchimp_lists = $mailchimp_lists->lists;
                    foreach ($mailchimp_lists as $mailchimp_list) {
                        ?>
                        <label><input type="checkbox" name="form_details[general][mailchimp_id][]" value="<?php echo esc_attr($mailchimp_list->id); ?>" class="stu-disable-checkbox-toggle" <?php echo (in_array($mailchimp_list->id, $selected_mailchimp_lists)) ? 'checked="checked"' : ''; ?>/><span><?php echo esc_attr($mailchimp_list->name); ?></span></label>
                        <?php
                    }
                } else {
                    esc_html_e('No list found for the mailchimp. Please check the mailchimp settings inside our settings section to fetch the mailchimp lists.', 'subscribe-to-unlock');
                }
                ?>
                <p class="description"><?php esc_html_e('Please check desired mailchimp list to send the subscribers directly to the mailchimp.', 'subscribe-to-unlock'); ?></p>
            </div>
        </div>
        <?php
    }
    if (!empty($stu_settings['constant_contact']['enable']) && !empty($stu_settings['constant_contact']['status']) && !empty($stu_settings['constant_contact']['constant_contact_lists'])) {
        ?>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Constant Contact Lists', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <?php
                $constant_contact_lists = json_decode($stu_settings['constant_contact']['constant_contact_lists']);
                $selected_constant_contact_lists = (!empty($form_details['general']['constant_contact_id'])) ? $form_details['general']['constant_contact_id'] : array();
                if (!empty($constant_contact_lists)) {
                    foreach ($constant_contact_lists as $constant_contact_list) {
                        ?>
                        <label class="stu-block"><input type="checkbox" name="form_details[general][constant_contact_id][]" value="<?php echo esc_attr($constant_contact_list->id); ?>" class="stu-disable-checkbox-toggle" <?php echo (in_array($constant_contact_list->id, $selected_constant_contact_lists)) ? 'checked="checked"' : ''; ?>/><span><?php echo esc_attr($constant_contact_list->name); ?></span></label>
                        <?php
                    }
                } else {
                    esc_html_e('No list found for the constant contact. Please check the constant contact settings inside our settings section to fetch the constant_contact lists.', 'subscribe-to-unlock');
                }
                ?>
                <p class="description"><?php esc_html_e('Please check desired constant contact list to send the subscribers directly to the constant contact.', 'subscribe-to-unlock'); ?></p>
            </div>
        </div>
        <?php
    }
    ?>
</div>

