<div class="stu-settings-each-section stu-display-none" data-tab="form">
    <div class="stu-field">
        <div class="stu-form-each-component">
            <div class="stu-component-head stu-clearfix">
                <h4><?php esc_html_e('Heading', 'subscribe-to-unlock'); ?></h4>
                <span class="dashicons dashicons-arrow-down"></span>
            </div>
            <div class="stu-component-body">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Show', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="checkbox" name="form_details[form][heading][show]" value="1" <?php echo (!empty($form_details['form']['heading']['show'])) ? 'checked="checked"' : ''; ?>/>
                    </div>
                </div>
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Heading Text', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <textarea  name="form_details[form][heading][text]"><?php echo (!empty($form_details['form']['heading']['text'])) ? $this->sanitize_html($form_details['form']['heading']['text']) : ''; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="stu-form-each-component">
            <div class="stu-component-head stu-clearfix">
                <h4><?php esc_html_e('Sub Heading', 'subscribe-to-unlock'); ?></h4>
                <span class="dashicons dashicons-arrow-down"></span>
            </div>
            <div class="stu-component-body">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Show', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="checkbox" name="form_details[form][sub_heading][show]" value="1" <?php echo (!empty($form_details['form']['sub_heading']['show'])) ? 'checked="checked"' : ''; ?>/>
                    </div>
                </div>
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Sub Heading Text', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <textarea name="form_details[form][sub_heading][text]"><?php echo (!empty($form_details['form']['sub_heading']['text'])) ? $this->sanitize_html($form_details['form']['sub_heading']['text']) : ''; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="stu-form-each-component">
            <div class="stu-component-head stu-clearfix">
                <h4><?php esc_html_e('Name', 'subscribe-to-unlock'); ?></h4>
                <span class="dashicons dashicons-arrow-down"></span>
            </div>
            <div class="stu-component-body">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Show', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="checkbox" name="form_details[form][name][show]" value="1" <?php echo (!empty($form_details['form']['name']['show'])) ? 'checked="checked"' : ''; ?>/>
                    </div>
                </div>
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Required', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="checkbox" name="form_details[form][name][required]" value="1" <?php echo (!empty($form_details['form']['name']['required'])) ? 'checked="checked"' : ''; ?>/>
                    </div>
                </div>

                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Label', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="text" name="form_details[form][name][label]" value="<?php echo (!empty($form_details['form']['name']['label'])) ? esc_attr($form_details['form']['name']['label']) : ''; ?>"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="stu-form-each-component">
            <div class="stu-component-head stu-clearfix">
                <h4><?php esc_html_e('Email', 'subscribe-to-unlock'); ?></h4>
                <span class="dashicons dashicons-arrow-down"></span>
            </div>
            <div class="stu-component-body">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Label', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="text" name="form_details[form][email][label]" value="<?php echo (!empty($form_details['form']['email']['label'])) ? esc_attr($form_details['form']['email']['label']) : ''; ?>"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="stu-form-each-component">
            <div class="stu-component-head stu-clearfix">
                <h4><?php esc_html_e('Country', 'subscribe-to-unlock'); ?></h4>
                <span class="dashicons dashicons-arrow-down"></span>
            </div>
            <div class="stu-component-body">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Required', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="checkbox" name="form_details[form][country][required]" value="1" <?php echo (!empty($form_details['form']['country']['required'])) ? 'checked="checked"' : ''; ?>/>
                    </div>
                </div>
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Label', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="text" name="form_details[form][country][label]" value="<?php echo (!empty($form_details['form']['country']['label'])) ? esc_attr($form_details['form']['country']['label']) : ''; ?>"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="stu-form-each-component">
            <div class="stu-component-head stu-clearfix">
                <h4><?php esc_html_e('Farm Size', 'subscribe-to-unlock'); ?></h4>
                <span class="dashicons dashicons-arrow-down"></span>
            </div>
            <div class="stu-component-body">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Required', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="checkbox" name="form_details[form][farm_size][required]" value="1" <?php echo (!empty($form_details['form']['farm_size']['required'])) ? 'checked="checked"' : ''; ?>/>
                    </div>
                </div>
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Label', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="text" name="form_details[form][farm_size][label]" value="<?php echo (!empty($form_details['form']['farm_size']['label'])) ? esc_attr($form_details['form']['farm_size']['label']) : ''; ?>"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="stu-form-each-component">
            <div class="stu-component-head stu-clearfix">
                <h4><?php esc_html_e('Terms and Agreement', 'subscribe-to-unlock'); ?></h4>
                <span class="dashicons dashicons-arrow-down"></span>
            </div>
            <div class="stu-component-body">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Show', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="checkbox" name="form_details[form][terms_agreement][show]" <?php echo (!empty($form_details['form']['terms_agreement']['show'])) ? 'checked="checked"' : ''; ?>/>
                    </div>
                </div>
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Text', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <textarea name="form_details[form][terms_agreement][agreement_text]"><?php echo (!empty($form_details['form']['terms_agreement']['agreement_text'])) ? $this->sanitize_html($form_details['form']['terms_agreement']['agreement_text']) : ''; ?></textarea>
                        <p class="description"><?php esc_html_e('You can enter basic html tags such as strong, a, ul, li etc.', 'subscribe-to-unlock'); ?></p>

                    </div>
                </div>
            </div>
        </div>
        <div class="stu-form-each-component">
            <div class="stu-component-head stu-clearfix">
                <h4><?php esc_html_e('Subscribe Button', 'subscribe-to-unlock'); ?></h4>
                <span class="dashicons dashicons-arrow-down"></span>
            </div>
            <div class="stu-component-body">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Button Text', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="text" name="form_details[form][subscribe_button][button_text]" placeholder="<?php esc_html_e('Subscribe', 'subscribe-to-unlock'); ?>" value="<?php echo (!empty($form_details['form']['subscribe_button']['button_text'])) ? esc_attr($form_details['form']['subscribe_button']['button_text']) : ''; ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="stu-form-each-component">
            <div class="stu-component-head stu-clearfix">
                <h4><?php esc_html_e('Footer', 'subscribe-to-unlock'); ?></h4>
                <span class="dashicons dashicons-arrow-down"></span>
            </div>
            <div class="stu-component-body">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Show', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <input type="checkbox" name="form_details[form][footer][show]" <?php echo (!empty($form_details['form']['footer']['show'])) ? 'checked="checked"' : ''; ?>/>
                    </div>
                </div>
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Footer Text', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <textarea name="form_details[form][footer][footer_text]"><?php echo (!empty($form_details['form']['footer']['footer_text'])) ? $this->sanitize_html($form_details['form']['footer']['footer_text']) : ''; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>