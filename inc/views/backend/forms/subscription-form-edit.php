<?php
defined('ABSPATH') or die('No script kiddies please!!');
$form_details = maybe_unserialize($form_row->form_details);
$preview_url = site_url() . '?stu_preview=true&form_alias=' . esc_attr($form_row->form_alias) . '&_wpnonce=' . wp_create_nonce('stu_form_preview_nonce');
?>
<div class="wrap stu-wrap">
    <div class="stu-header stu-clearfix">
        <h1 class="stu-floatLeft">
            <img src="<?php echo STU_URL . 'images/logo.png' ?>" class="stu-plugin-logo" />
            <span class="stu-sub-header"><?php esc_html_e('Edit Subscription Form', 'subscribe-to-unlock'); ?></span>
        </h1>
        <div class="stu-add-wrap">
            <a href="javascript:void(0);" class="stu-form-save-trigger"><input type="button" class="stu-button-white" value="<?php esc_html_e('Save', 'subscribe-to-unlock'); ?>"></a>
            <a href="<?php echo esc_url($preview_url); ?>" target="_blank"><input type="button" class="stu-button-orange" value="<?php esc_html_e('Preview', 'subscribe-to-unlock'); ?>"></a>
            <a href="<?php echo admin_url('admin.php?page=subscribe-to-unlock'); ?>"><input type="button" class="stu-button-red" value="<?php esc_html_e('Cancel', 'subscribe-to-unlock'); ?>"></a>
        </div>
    </div>
    <div class="stu-form-wrap stu-form-add-block stu-clearfix">
        <form method="post" action="" class="stu-subscription-form">
            <input type="hidden" name="form_id" value="<?php echo intval($form_row->form_id); ?>"/>
            <?php
            /**
             * Navigation Menu
             */
            include(STU_PATH . 'inc/views/backend/form-sections/navigation.php');
            ?>
            <div class="stu-settings-section-wrap">
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Shortcode', 'subscribe-to-unlock') ?></label>
                    <div class="stu-field">
                        <span class="stu-shortcode-preview">[stu alias="<?php echo esc_attr($form_row->form_alias); ?>"]</span>
                        <span class="stu-clipboard-copy"><i class="fas fa-clipboard-list"></i></span>
                    </div>
                </div>
                <?php
                /**
                 * General Settings
                 */
                include(STU_PATH . 'inc/views/backend/form-sections/general-settings.php');
                ?>
                <?php
                /**
                 * Form Settings
                 */
                include(STU_PATH . 'inc/views/backend/form-sections/form-settings.php');
                ?>
                <?php
                /**
                 * Layout Settings
                 */
                include(STU_PATH . 'inc/views/backend/form-sections/layout-settings.php');
                ?>
                <?php
                /**
                 * Email Settings
                 */
                include(STU_PATH . 'inc/views/backend/form-sections/email-settings.php');
                ?>
                <?php
                /**
                 * Custom Settings
                 */
                include(STU_PATH . 'inc/views/backend/form-sections/custom-settings.php');
                ?>
            </div>

        </form>
    </div>
</div>
<div class="stu-form-message"></div>