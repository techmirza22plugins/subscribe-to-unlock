<?php
defined('ABSPATH') or die('No script kiddies please!!');
?>
<div class="wrap stu-wrap">
    <div class="stu-header stu-clearfix">
        <h1 class="stu-floatLeft">
            <img src="<?php echo STU_URL . 'images/logo.png' ?>" class="stu-plugin-logo" />
            <span class="stu-sub-header"><?php esc_html_e('Subscription Forms', 'subscribe-to-unlock'); ?></span>
        </h1>
        <div class="stu-add-wrap">
            <a href="<?php echo admin_url('admin.php?page=stu-add-subscription-form'); ?>"><input type="button" class="stu-button-white" value="<?php esc_html_e('Add New Form', 'subscribe-to-unlock'); ?>"></a>
        </div>
    </div>
    <div class="stu-form-wrap">

        <table class="wp-list-table widefat fixed stu-form-lists-table">
            <thead>
                <tr>
                    <th><?php esc_html_e('Form Title', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Alias', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Shortcode', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Status', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Action', 'subscribe-to-unlock'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                global $wpdb;
                $form_table = STU_FORM_TABLE;
                $form_rows = $wpdb->get_results("select * from $form_table order by form_title asc");
                if (!empty($form_rows)) {
                    foreach ($form_rows as $form_row) {
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo admin_url('admin.php?page=subscribe-to-unlock&form_id=' . $form_row->form_id . '&action=edit_form'); ?>" title="<?php esc_attr_e('Edit Form', 'subscribe-to-unlock'); ?>"><?php echo esc_attr($form_row->form_title); ?></a>
                            </td>
                            <td><?php echo esc_attr($form_row->form_alias); ?></td>
                            <td>
                                <span class="stu-shortcode-preview">[stu alias="<?php echo esc_attr($form_row->form_alias); ?>"]</span>
                                <span class="stu-clipboard-copy"><i class="fas fa-clipboard-list"></i></span>
                            </td>
                            <td><?php echo (!empty($form_row->form_status)) ? esc_html__('Active', 'subscribe-to-unlock') : esc_html__('Inactive', 'subscribe-to-unlock'); ?></td>
                            <td>
                                <a class="stu-edit" href="<?php echo admin_url('admin.php?page=subscribe-to-unlock&form_id=' . $form_row->form_id . '&action=edit_form'); ?>" title="<?php esc_attr_e('Edit Form', 'subscribe-to-unlock'); ?>"><?php esc_html_e('Edit', 'subscribe-to-unlock'); ?></a>
                                <a class="stu-copy stu-form-copy" href="javascript:void(0);" data-form-id="<?php echo intval($form_row->form_id); ?>" title="<?php esc_attr_e('Copy Form', 'subscribe-to-unlock'); ?>"><?php esc_html_e('Copy', 'subscribe-to-unlock'); ?></a>
                                <a class="stu-preview" href="<?php echo site_url() . '?stu_preview=true&form_alias=' . esc_attr($form_row->form_alias) . '&_wpnonce=' . wp_create_nonce('stu_form_preview_nonce'); ?>" target="_blank" title="<?php esc_attr_e('Preview', 'subscribe-to-unlock'); ?>"><?php esc_html_e('Preview', 'subscribe-to-unlock'); ?></a>
                                <a class="stu-delete stu-form-delete" href="javascript:void(0)" data-form-id="<?php echo intval($form_row->form_id); ?>" title="<?php esc_attr_e('Delete Form', 'subscribe-to-unlock'); ?>"><?php esc_html_e('Delete', 'subscribe-to-unlock'); ?></a>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="5"><?php esc_html_e('No forms added yet.', 'subscribe-to-unlock'); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <th><?php esc_html_e('Form Title', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Alias', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Shortcode', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Status', 'subscribe-to-unlock'); ?></th>
                    <th><?php esc_html_e('Action', 'subscribe-to-unlock'); ?></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<div class="stu-form-message"></div>
