<div class="wrap stu-wrap">
    <div class="stu-header stu-clearfix">
        <h1 class="stu-floatLeft">
            <img src="<?php echo STU_URL . 'images/logo.png' ?>" class="stu-plugin-logo" />
            <span class="stu-sub-header"><?php esc_html_e('Help', 'subscribe-to-unlock'); ?></span>
        </h1>
        <div class="stu-add-wrap">
            <a href="<?php echo admin_url('admin.php?page=stu-add-subscription-form'); ?>"><input type="button" class="stu-button-white" value="<?php esc_html_e('Add New Form', 'subscribe-to-unlock'); ?>"></a>
        </div>
    </div>

    <div class="stu-form-wrap stu-form-add-block stu-clearfix">
        <div class="stu-content-block">
            <h2><?php esc_html_e('Documentation', 'subscribe-to-unlock'); ?></h2>
            <p><?php _e(sprintf('You can check our detailed documentation %s here %s', '<a href="https://wpshuffle.com/wordpress-documentations/subscribe-to-unlock/" target="_blank">', '</a>'), 'subscribe-to-unlock'); ?></p>
        </div>

        <div class="stu-content-block">
            <h2><?php esc_html_e('Support', 'subscribe-to-unlock'); ?></h2>
            <p><?php esc_html_e('In case you need any assistance regarding our plugin then you can contact us from below link. We will try to get back to you as soon as possible.', 'subscribe-to-unlock'); ?></p>
            <a href="https://codecanyon.net/user/wpshuffle#contact" target="_blank">https://codecanyon.net/user/wpshuffle#contact</a>
        </div>

        <div class="stu-content-block">
            <h2><?php esc_html_e('Developer Documentation', 'subscribe-to-unlock'); ?></h2>
            <p><?php esc_html_e('If you are developer and trying to add any functionality or customize our plugin through hooks then below are the list of actions and filters available in the plugin.', 'subscribe-to-unlock'); ?></p>
            <div class="stu-content-sub-block">
                <h3><?php esc_html_e('Available Actions', 'subscribe-to-unlock'); ?></h3>
                <div class="stu-hooks-wrap">
                    <pre>
/**
 * <?php esc_html_e('Fires when Init hook is fired through plugin', 'subscribe-to-unlock'); ?>
 *
 * @since 1.0.0
 */
do_action('stu_init');
                    </pre>
                    <pre>
/**
 * <?php esc_html_e('Triggers just before processing the subscription form', 'subscribe-to-unlock'); ?>
 *
 * @since 1.0.0
 */
do_action('stu_before_form_process');
                    </pre>
                    <pre>
/**
 * <?php esc_html_e('Triggers at the end of processing the subscription form successfully', 'subscribe-to-unlock'); ?>
 *
 * @param array $form_data
 *
 * @since 1.0.0
 */
 do_action('stu_end_form_process', $form_data, $form_details);
                    </pre>
                    <pre>
/**
 * <?php esc_html_e('Triggers just before displaying the subscription form', 'subscribe-to-unlock'); ?>
 *
 * @param object $form_row
 *
 * @since 1.0.0
 */
 do_action('stu_before_form', $form_row);
                    </pre>
                    <pre>
/**
 * <?php esc_html_e('Triggers just after displaying the subscription form', 'subscribe-to-unlock'); ?>
 *
 * @param object $form_row
 *
 * @since 1.0.0
 */
do_action('stu_after_form', $form_row);
                    </pre>

                </div>
            </div>
            <div class="stu-content-sub-block">
                <h3><?php esc_html_e('Available Filters', 'subscribe-to-unlock'); ?></h3>
                <div class="stu-hooks-wrap">
                    <pre>
/**
 * <?php esc_html_e('Filters csv rows', 'subscribe-to-unlock'); ?>
 *
 * @param array $csv_rows
 *
 * @since 1.0.0
 */
$csv_rows = apply_filters('stu_csv_rows', $csv_rows);
                    </pre>

                    <pre>
/**
 * <?php esc_html_e('Filters post parameters being sent to Maichimp', 'subscribe-to-unlock'); ?>
 *
 * @param array $post_parameters
 * @param array $form_data
 * @param array $form_details
 */
 $post_parameters = apply_filters('stu_mc_post_parameters', $post_parameters, $form_data, $form_details);
                    </pre>
                    <pre>
/**
 * <?php esc_html_e('Filters parameters being sent to Constant Contact', 'subscribe-to-unlock'); ?>
 *
 * @param array $post_parameters
 * @param array $form_data
 * @param array $form_details
 */
 $post_parameters = apply_filters('stu_cc_post_parameters', $post_parameters, $form_data, $form_details);
                    </pre>
                    <pre>
/**
 * <?php esc_html_e('Filters email message', 'subscribe-to-unlock'); ?>
 *
 * @param string $email_message
 * @param array $form_data
 *
 * @since 1.0.0
 */
$email_message = apply_filters('stu_email_message', $email_message, $form_data);
                    </pre>
                </div>
            </div>
            <p><?php esc_html_e('If you think there are any missing action or filters then please let us know from below link.', 'subscribe-to-unlock'); ?></p>
            <a href="https://codecanyon.net/user/wpshuffle#contact" target="_blank">https://codecanyon.net/user/wpshuffle#contact</a>
        </div>

    </div>
</div>
