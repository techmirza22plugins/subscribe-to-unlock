<# if(data.length>0) { #>
<div class="stu-field-wrap">
    <label><?php esc_html_e('Subscription Lists', 'subscribe-to-unlock'); ?></label>
    <div class="stu-field">
        <div class="stu-custom-table">
        <div class="stu-each-list stu-list-head">
            <span class="stu-list-col"><?php esc_html_e('ID', 'subscribe-to-unlock'); ?></span>
            <span class="stu-list-col"><?php esc_html_e('List Name', 'subscribe-to-unlock'); ?></span>
        </div>
        <#   for ( var i=0;i<data.length;i++ ) { #>
            <div class="stu-each-list">
                <span class="stu-list-col">{{data[i].id}}</span>
                <span class="stu-list-col">{{data[i].name}}</span>
            </div>
            <# } #>
            </div>
    </div>
</div>
<# } #>