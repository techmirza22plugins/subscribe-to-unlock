<div class="wrap stu-wrap">
    <div class="stu-header stu-clearfix">
        <h1 class="stu-floatLeft">
            <img src="<?php echo STU_URL . 'images/logo.png' ?>" class="stu-plugin-logo" />
            <span class="stu-sub-header"><?php esc_html_e('About', 'subscribe-to-unlock'); ?></span>
        </h1>
        <div class="stu-add-wrap">
            <a href="<?php echo admin_url('admin.php?page=stu-add-subscription-form'); ?>"><input type="button" class="stu-button-white" value="<?php esc_html_e('Add New Form', 'subscribe-to-unlock'); ?>"></a>
        </div>
    </div>

    <div class="stu-form-wrap stu-form-add-block stu-clearfix">

        <div class="stu-content-block">
            <h2><?php esc_html_e('About Plugin', 'subscribe-to-unlock'); ?></h2>
            <p><?php esc_html_e("As the name explains, this plugin makes it fast and easy to capture subscribers right from your WordPress site by simply locking some specific content of your site until users subscribe to your site. You can even lock the whole page with popup or give a countdown to users until they can view the content without subscribing.", 'subscribe-to-unlock'); ?></p>
            <p><?php esc_html_e("You can create unlimited locker forms, choose a stunning layout from our 10 beautifully pre designed templates, connect the forms directly to popular third party subscription services such as Mailchimp and Constant Contact, export subscribers and what not. With this plugin you are just few seconds away from collecting the subscribers from your WordPress site because creating and integrating a subscription form in any site is that easy with our plugin.", 'subscribe-to-unlock'); ?></p>
        </div>

        <div class="stu-content-block">
            <h2><?php esc_html_e('Features', 'subscribe-to-unlock'); ?></h2>
            <ul class="stu-bullets">
                <li><?php esc_html_e('Unlimited Locker Forms', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('10 Pre Designed Subscription Form Templates', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Initiative lock button', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Popup Page Locker', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Popup Page Locker with Countdown', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Two Locker Modes - Hard and Soft Lock', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Two Email verification methods available - Link or Unlock Code', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Mailchimp and Constant Contact integrated', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Inbuilt Form Style Customizer', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Enable disable each form components', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Ajax Form Submission', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Subscribers CSV Export', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Backend Form Preview', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Enable/disable form components', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('All device friendly and brower Compatibility', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Dedicated Support', 'subscribe-to-unlock'); ?></li>
                <li><?php esc_html_e('Translation Ready', 'subscribe-to-unlock'); ?></li>

            </ul>
        </div>

        <div class="stu-content-block">
            <h2><?php esc_html_e('About WP Shuffle', 'subscribe-to-unlock'); ?></h2>
            <p><?php esc_html_e('We are a bunch of WordPress Enthusiasts with an aim to develop the WordPress Themes and Plugins that adds a value to any WordPress site. Our mission is to create a WordPress product that is easy to use, highly customizable and offers innovative features that are useful for every another WordPress site.', 'subscribe-to-unlock'); ?></p>
            <p><?php esc_html_e('If talking about support, we value our customers more that we value our products so our qualified support team is always there to provide any assistance with our products.', 'subscribe-to-unlock'); ?></p>
        </div>

        <div class="stu-content-block">
            <h2><?php esc_html_e('Our Themes', 'subscribe-to-unlock'); ?></h2>
            <a href="https://wpshuffle.com/wordpress-themes">https://wpshuffle.com/wordpress-themes/</a>
        </div>

        <div class="stu-content-block">
            <h2><?php esc_html_e('Our Plugins', 'subscribe-to-unlock'); ?></h2>
            <a href="https://wpshuffle.com/wordpress-plugins">https://wpshuffle.com/wordpress-plugins/</a>
        </div>

    </div>
</div>
