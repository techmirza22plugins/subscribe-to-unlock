<div class="stu-tinymce-popup">
    <div class="stu-tinymce-overlay"></div>
    <div class="stu-tinymce-popup-content">
        <h3><?php esc_html_e('Subscribe to Unlock', 'subscribe-to-unlock'); ?><span class="dashicons dashicons-no-alt stu-tinymce-popup-close"></span></h3>
        <select id="stu-form-lists">
            <option value=""><?php esc_html_e('Choose Form', 'subscribe-to-unlock'); ?></option>
            <?php
            global $wpdb;
            $subscription_form_table = STU_FORM_TABLE;
            $stu_forms = $wpdb->get_results("select * from $subscription_form_table order by form_title asc");
            if (!empty($stu_forms)) {
                foreach ($stu_forms as $stu_form) {
                    ?>
                    <option value="<?php echo esc_attr($stu_form->form_alias); ?>"><?php echo esc_attr($stu_form->form_title); ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <input type="button" class="stu-shortcode-inserter button-secondary" value="<?php esc_html_e('Lock Content', 'subscribe-to-unlock'); ?>">
    </div>
</div>
<input type="hidden" id="stu-tinymce-icon" value="<?php echo STU_URL . 'images/tinymce-lock-icon.png' ?>"/>
<input type="hidden" id="stu-tinymce-icon-title" value="<?php esc_html_e('Subscribe to Unlock', 'subscribe-to-unlock'); ?>"/>
<input type="hidden" id="stu-tinymce-error-message" value="<?php esc_html_e('Please select some text to lock.', 'subscribe-to-unlock'); ?>"/>