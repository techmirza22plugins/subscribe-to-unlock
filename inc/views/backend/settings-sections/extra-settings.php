<div class="stu-settings-each-section stu-display-none" data-tab="extra">
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Test Mode', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="stu_settings[extra][test_mode]" value="1" <?php echo (!empty($stu_settings['extra']['test_mode'])) ? 'checked="checked"' : ''; ?>/>
            <p class="description"><?php esc_html_e('Please enable test mode which will show the lockers despite of unlocking. This is useful for administrators to test the lockers.', 'subscribe-to-unlock'); ?></p>
            <p class="description"><?php esc_html_e('Please note that this option will only work if you are logged in.', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Disable Fontawesome CSS', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="stu_settings[extra][disable_fontawesome]" value="1" <?php echo (!empty($stu_settings['extra']['disable_fontawesome'])) ? 'checked="checked"' : ''; ?>/>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Unsubscribe Message', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <textarea name="stu_settings[extra][unsubscribe_message]"><?php echo (!empty($stu_settings['extra']['unsubscribe_message'])) ? $this->output_converting_br($stu_settings['extra']['unsubscribe_message']) : $this->default_unsubscribe_message(); ?></textarea>
            <p class="description"><?php esc_html_e('This message is displayed when user clicks the unsubscribe link and you can generate the unsubscribe link for individual subscriber in the subscribers list section of our plugin.', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Lock Index Page', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="stu_settings[extra][lock_index_page]" value="1" class="stu-checkbox-toggle-trigger" data-toggle-class="stu-index-page-locker-ref" <?php echo (!empty($stu_settings['extra']['lock_index_page'])) ? 'checked="checked"' : ''; ?>/>
            <p class="description"><?php esc_html_e('Please check if you want to lock the index page with a locker form. Please note this will only work if you have your home page displays as "Your Latest Post".', 'subscribe-to-unlock'); ?></p>
        </div>
    </div>
    <div class="stu-index-page-locker-ref" <?php echo (empty($stu_settings['extra']['lock_index_page'])) ? 'style="display:none"' : ''; ?>>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Countdown Timer', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input type="number" name="stu_settings[extra][countdown_timer]" min="0" value="<?php echo (!empty($stu_settings['extra']['countdown_timer'])) ? intval($stu_settings['extra']['countdown_timer']) : ''; ?>"/>
                <p class="description"><?php esc_html_e('Please enter the countdown timer for popup display in seconds. Please leave empty or enter 0 to disable countdown timer.', 'subscribe-to-unlock'); ?></p>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Countdown Timer Message', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <textarea name="stu_settings[extra][countdown_timer_message]"><?php echo (!empty($stu_settings['extra']['countdown_timer_message'])) ? esc_attr($stu_settings['extra']['countdown_timer_message']) : ''; ?></textarea>
                <p class="description"><?php esc_html_e('Please use #countdown to replace it with the actual countdown timer in the message.', 'subscribe-to-unlock'); ?></p>
                <p class="description"><?php esc_html_e('For eg: Please subscribe to unlock the page or wait #countdown seconds.', 'subscribe-to-unlock'); ?></p>
            </div>

        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Subscription Form', 'subscribe-to-unlock') ?></label>
            <div class="stu-field">
                <select name="stu_settings[extra][form_alias]">
                    <option value=""><?php esc_html_e("Choose Form", 'subscribe-to-unlock'); ?></option>
                    <?php
                    $selected_form_alias = (!empty($stu_settings['extra']['form_alias'])) ? $stu_settings['extra']['form_alias'] : '';
                    global $wpdb;
                    $form_table = STU_FORM_TABLE;
                    $forms = $wpdb->get_results("select * from $form_table order by form_title asc");
                    if (!empty($forms)) {
                        foreach ($forms as $form) {
                            ?>
                            <option value="<?php echo esc_attr($form->form_alias); ?>" <?php selected($selected_form_alias, $form->form_alias); ?>><?php echo esc_attr($form->form_title); ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>