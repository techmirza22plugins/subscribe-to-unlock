<div class="stu-settings-each-section stu-display-none" data-tab="constant_contact">
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Enable Constant Contact', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="stu_settings[constant_contact][enable]" value="1" <?php echo (!empty($stu_settings['constant_contact']['enable'])) ? 'checked="checked"' : ''; ?> class="stu-checkbox-toggle-trigger" data-toggle-class="stu-cc-enabled"/>
        </div>
    </div>
    <?php $enable_constant_contact = (!empty($stu_settings['constant_contact']['enable'])) ? 1 : 0; ?>
    <div class="stu-cc-enabled" <?php $this->display_none($enable_constant_contact, 1); ?>>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Constant Contact API Key', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input type="text" name="stu_settings[constant_contact][api_key]" value="<?php echo (!empty($stu_settings['constant_contact']['api_key'])) ? esc_attr($stu_settings['constant_contact']['api_key']) : ''; ?>" class="stu-constant_contact-api-key"/>
                <p class="description"><?php echo sprintf(esc_html__('Please go %s here %s to generate your api keys', 'subscribe-to-unlock'), '<a href="https://constantcontact.mashery.com/apps/mykeys" target="_blank">', '</a>'); ?></p>
                <p class="description"><?php echo sprintf(esc_html__('Please go %s here %s to know how to generate necessary keys', 'subscribe-to-unlock'), '<a href="https://developer.constantcontact.com/home/api-keys.html" target="_blank">', '</a>'); ?></p>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Constant Contact Access Token', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input type="text" name="stu_settings[constant_contact][access_token]" value="<?php echo (!empty($stu_settings['constant_contact']['access_token'])) ? esc_attr($stu_settings['constant_contact']['access_token']) : ''; ?>" class="stu-constant_contact-access-token"/>
                <p class="description"><?php echo sprintf(esc_html__('Please go %s here %s to generate your access token.', 'subscribe-to-unlock'), '<a href="https://constantcontact.mashery.com/apps/mykeys" target="_blank">', '</a>'); ?></p>
                <p class="description"><?php echo sprintf(esc_html__('Please go %s here %s to know how to generate necessary keys', 'subscribe-to-unlock'), '<a href="https://developer.constantcontact.com/home/api-keys.html" target="_blank">', '</a>'); ?></p>


            </div>
        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Status', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <?php
                $constant_contact_status = (!empty($stu_settings['constant_contact']['status'])) ? 1 : 0;
                $constant_contact_status_class = ($constant_contact_status == 1) ? 'stu-cc-connected' : 'stu-cc-disconnected';
                ?>
                <input type="hidden" name="stu_settings[constant_contact][status]" value="<?php echo intval($constant_contact_status); ?>" class="stu-constant_contact-status-flag"/>

                <span class="stu-constant_contact-status <?php echo esc_attr($constant_contact_status_class); ?>"><?php echo ($constant_contact_status == 1) ? esc_html__('Connected', 'subscribe-to-unlock') : esc_html__('Disconnected', 'subscribe-to-unlock'); ?></span>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label></label>
            <div class="stu-field">
                <?php
                $connect_label = (!empty($constant_contact_status)) ? esc_html__('Renew List', 'subscribe-to-unlock') : esc_html__('Connect', 'subscribe-to-unlock');
                ?>
                <input type="button" class="stu-constant_contact-connect button-secondary" value="<?php echo esc_attr($connect_label); ?>">
                <?php if ($constant_contact_status == 1) {
                    ?>
                    <input type="button" class="stu-constant_contact-reset button-secondary" value="<?php esc_html_e("Reset", 'subscribe-to-unlock'); ?>">
                    <?php
                }
                ?>
                <div class = "stu-constant_contact-api-response stu-display-none"></div>
            </div>
        </div>
        <div class = "stu-constant_contact-lists-wrap">
            <?php
            if (!empty($stu_settings['constant_contact']['constant_contact_lists'])) {
                $constant_contact_lists = json_decode($stu_settings['constant_contact']['constant_contact_lists']);
                ?>
                <div class="stu-field-wrap stu-custom-table">
                    <label><?php esc_html_e('Constant Contact Lists', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <div class="stu-custom-table">
                            <div class="stu-each-list stu-list-head">
                                <span class="stu-list-col"><?php esc_html_e('ID', 'subscribe-to-unlock'); ?></span>
                                <span class="stu-list-col"><?php esc_html_e('List Name', 'subscribe-to-unlock'); ?></span>
                            </div>
                            <?php
                            foreach ($constant_contact_lists as $constant_contact_list) {
                                ?>
                                <div class="stu-each-list">
                                    <span class="stu-list-col"><?php echo esc_attr($constant_contact_list->id); ?></span>
                                    <span class="stu-list-col"><?php echo esc_attr($constant_contact_list->name); ?></span>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <textarea name="stu_settings[constant_contact][constant_contact_lists]" class="stu-constant_contact-lists-input stu-display-none"><?php echo (!empty($stu_settings['constant_contact']['constant_contact_lists'])) ? $this->sanitize_html($stu_settings['constant_contact']['constant_contact_lists']) : ''; ?></textarea>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Last Log', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <textarea name="stu_settings[constant_contact][cc_log]"><?php echo (!empty($stu_settings['constant_contact']['cc_log'])) ? $this->sanitize_html($stu_settings['constant_contact']['cc_log']) : ''; ?></textarea>
                <input type="button" class="stu-clear-log-trigger button-secondary" value="<?php esc_html_e('Clear Log', 'subscribe-to-unlock'); ?>">
            </div>
        </div>
    </div>
</div>
