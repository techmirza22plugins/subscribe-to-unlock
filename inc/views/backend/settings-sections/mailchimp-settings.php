<div class="stu-settings-each-section" data-tab="mailchimp">
    <div class="stu-field-wrap">
        <label><?php esc_html_e('Enable Mailchimp', 'subscribe-to-unlock'); ?></label>
        <div class="stu-field">
            <input type="checkbox" name="stu_settings[mailchimp][enable]" value="1" <?php echo (!empty($stu_settings['mailchimp']['enable'])) ? 'checked="checked"' : ''; ?> class="stu-checkbox-toggle-trigger" data-toggle-class="stu-mc-enabled"/>
        </div>
    </div>
    <?php $enable_mailchimp = (!empty($stu_settings['mailchimp']['enable'])) ? 1 : 0; ?>
    <div class="stu-mc-enabled" <?php $this->display_none($enable_mailchimp, 1); ?>>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Mailchimp API Key', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <input type="text" name="stu_settings[mailchimp][api_key]" value="<?php echo (!empty($stu_settings['mailchimp']['api_key'])) ? esc_attr($stu_settings['mailchimp']['api_key']) : ''; ?>" class="stu-mailchimp-api-key"/>
                <p class="description"><?php echo sprintf(esc_html__('Please go %s here %s to generate your api keys', 'subscribe-to-unlock'), '<a href="https://mailchimp.com/help/about-api-keys/#Find_or_Generate_Your_API_Key" target="_blank">', '</a>'); ?></p>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Status', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <?php
                $mailchimp_status = (!empty($stu_settings['mailchimp']['status'])) ? 1 : 0;
                $mailchimp_status_class = ($mailchimp_status == 1) ? 'stu-mc-connected' : 'stu-mc-disconnected';
                ?>
                <input type="hidden" name="stu_settings[mailchimp][status]" value="<?php echo intval($mailchimp_status); ?>" class="stu-mailchimp-status-flag"/>

                <span class="stu-mailchimp-status <?php echo esc_attr($mailchimp_status_class); ?>"><?php echo ($mailchimp_status == 1) ? esc_html__('Connected', 'subscribe-to-unlock') : esc_html__('Disconnected', 'subscribe-to-unlock'); ?></span>
            </div>
        </div>
        <div class="stu-field-wrap">
            <label></label>
            <div class="stu-field">
                <?php
                $connect_label = (!empty($mailchimp_status)) ? esc_html__('Renew List', 'subscribe-to-unlock') : esc_html__('Connect', 'subscribe-to-unlock');
                ?>
                <input type="button" class="stu-mailchimp-connect button-secondary" value="<?php echo esc_attr($connect_label); ?>">
                <?php if ($mailchimp_status == 1) {
                    ?>
                    <input type="button" class="stu-mailchimp-reset button-secondary" value="<?php esc_html_e("Reset", 'subscribe-to-unlock'); ?>">
                    <?php
                }
                ?>
                <div class = "stu-mailchimp-api-response stu-display-none"></div>
            </div>
        </div>
        <div class = "stu-mailchimp-lists-wrap">
            <?php
            if (!empty($stu_settings['mailchimp']['mailchimp_lists'])) {
                $mailchimp_lists = json_decode($stu_settings['mailchimp']['mailchimp_lists']);
                ?>
                <div class="stu-field-wrap">
                    <label><?php esc_html_e('Mailchimp Lists', 'subscribe-to-unlock'); ?></label>
                    <div class="stu-field">
                        <div class="stu-custom-table">
                            <div class="stu-each-list stu-list-head">
                                <span class="stu-list-col"><?php esc_html_e('ID', 'subscribe-to-unlock'); ?></span>
                                <span class="stu-list-col"><?php esc_html_e('List Name', 'subscribe-to-unlock'); ?></span>
                            </div>
                            <?php
                            foreach ($mailchimp_lists->lists as $mailchimp_list) {
                                ?>
                                <div class="stu-each-list">
                                    <span class="stu-list-col"><?php echo esc_attr($mailchimp_list->id); ?></span>
                                    <span class="stu-list-col"><?php echo esc_attr($mailchimp_list->name); ?></span>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <textarea name="stu_settings[mailchimp][mailchimp_lists]" class="stu-mailchimp-lists-input stu-display-none"><?php echo (!empty($stu_settings['mailchimp']['mailchimp_lists'])) ? $this->sanitize_html($stu_settings['mailchimp']['mailchimp_lists']) : ''; ?></textarea>
        <div class="stu-field-wrap">
            <label><?php esc_html_e('Last Log', 'subscribe-to-unlock'); ?></label>
            <div class="stu-field">
                <textarea name="stu_settings[mailchimp][mc_log]"><?php echo (!empty($stu_settings['mailchimp']['mc_log'])) ? $this->sanitize_html($stu_settings['mailchimp']['mc_log']) : ''; ?></textarea>
                <input type="button" class="stu-clear-log-trigger button-secondary" value="<?php esc_html_e('Clear Log', 'subscribe-to-unlock'); ?>">
            </div>
        </div>
    </div>
</div>
