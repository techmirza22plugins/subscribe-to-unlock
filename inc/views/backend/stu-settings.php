<?php
defined('ABSPATH') or die('No script kiddies please!!');
$stu_settings = get_option('stu_settings');
?>
<div class="wrap stu-wrap">
    <div class="stu-header stu-clearfix">
        <h1 class="stu-floatLeft">
            <img src="<?php echo STU_URL . 'images/logo.png' ?>" class="stu-plugin-logo" />
            <span class="stu-sub-header"><?php esc_html_e('Settings', 'subscribe-to-unlock'); ?></span>
        </h1>
        <div class="stu-add-wrap">
            <a href="javascript:void(0);" class="stu-form-save-trigger"><input type="button" class="stu-button-white" value="<?php esc_html_e('Save Settings', 'subscribe-to-unlock'); ?>"></a>
        </div>
    </div>

    <div class="stu-form-wrap stu-form-add-block stu-clearfix">
        <form method="post" action="" class="stu-subscription-form" data-form-action="stu_settings_save_action">
            <div class="stu-form-nav-wrap">
                <ul class="stu-form-nav">
                    <li><a href="javascript:void(0);" class="stu-nav-item stu-active-nav" data-tab="mailchimp"><span class="dashicons dashicons-email"></span><?php esc_html_e('Mailchimp Settings', 'subscribe-to-unlock'); ?></a></li>
                    <li><a href="javascript:void(0);" class="stu-nav-item" data-tab="constant_contact"><span class="dashicons dashicons-email-alt"></span><?php esc_html_e('Constant Contact Settings', 'subscribe-to-unlock'); ?></a></li>
                    <li><a href="javascript:void(0);" class="stu-nav-item" data-tab="extra"><span class="dashicons dashicons-admin-settings"></span><?php esc_html_e('Extra Settings', 'subscribe-to-unlock'); ?></a></li>
                </ul>
            </div>
            <div class="stu-settings-section-wrap">
                <?php
                /**
                 * Mailchimp Settings
                 */
                include(STU_PATH . 'inc/views/backend/settings-sections/mailchimp-settings.php');
                ?>
                <?php
                /**
                 * Constant Contact Settings
                 */
                include(STU_PATH . 'inc/views/backend/settings-sections/constant-contact-settings.php');
                ?>
                <?php
                /**
                 * Extra Settings
                 */
                include(STU_PATH . 'inc/views/backend/settings-sections/extra-settings.php');
                ?>

            </div>

        </form>
    </div>
</div>
<div class="stu-form-message"></div>