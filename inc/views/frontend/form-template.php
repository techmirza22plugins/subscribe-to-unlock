<div class="stu-main-outer-wrap <?php echo (!isset($atts['page_lock'])) ? 'stu-content-locked' : ''; ?> <?php echo esc_attr($lock_mode_class); ?>">
    <div class="stu-blur-overlay">
        <div class="stu-form-wrap stu-<?php echo esc_attr($form_template); ?> stu-<?php echo esc_attr($form_row->form_alias); ?>">
            <form method="post" action="" class="stu-subscription-form" data-form-alias="<?php echo esc_attr($form_row->form_alias); ?>">
                <?php
                $after_verification = (!empty($form_details['general']['after_verification'])) ? $form_details['general']['after_verification'] : 'show_unlock_message';
                if ($after_verification == 'redirect_form_page') {
                    ?>
                    <input type="hidden" name="stu_redirect_url" value="<?php echo esc_url($this->get_current_page_url()); ?>"/>
                    <?php
                }
                ?>
                <?php
                /**
                 * Triggers just before displaying the subscription form
                 *
                 * @param object $form_row
                 *
                 * @since 1.0.0
                 */
                do_action('stu_before_form', $form_row);

                if (file_exists(STU_PATH . 'inc/views/frontend/form-templates/' . $form_template . '.php')) {
                    include(STU_PATH . 'inc/views/frontend/form-templates/' . $form_template . '.php');
                }

                /**
                 * Triggers just after displaying the subscription form
                 *
                 * @param object $form_row
                 *
                 * @since 1.0.0
                 */
                do_action('stu_after_form', $form_row);
                ?>
            </form>
            <?php
            if (!empty($form_details['general']['verification']) && $form_details['general']['verification_type'] == 'unlock_code') {
                ?>
                <div class="stu-unlock-form-wrap">
                    <div class="stu-unlock-label"><?php echo esc_attr($form_details['general']['unlock_message']); ?></div>
                    <input type="text" class="stu-unlock-code-field"/>
                    <input type="button" class="stu-unlock-button stu-form-submit" value="<?php echo esc_attr($form_details['general']['unlock_button_label']) ?>"/>
                    <div class="stu-unlock-error-message"><span><?php echo $this->sanitize_html($form_details['general']['unlock_error_message']); ?></span></div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php if (!isset($atts['page_lock'])) { ?>
        <div class="stu-lock-content" <?php $this->display_none($lock_mode, 'soft'); ?>>
            <?php
            if (empty($content)) {
                echo do_shortcode($this->sanitize_html($form_details['general']['lock_content']));
            } else {
                echo do_shortcode($content);
            }
            ?>
        </div>
    <?php } ?>
</div>
