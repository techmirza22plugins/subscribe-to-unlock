<?php
defined('ABSPATH') or die('No script kiddies please!!');
$form_alias = sanitize_text_field($_GET['form_alias']);
$form_row = $this->get_form_row_by_alias($form_alias);
if (empty($form_row)) {
    return;
}
$form_alias = $form_row->form_alias;
$form_id = $form_row->form_id;
?>
<html>
    <head>
        <title><?php esc_html_e('Preview - Subscribe to Download', 'subscribe-to-unlock'); ?></title>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div class="stu-preview-head-wrap">
            <div class="stu-preview-title-wrap">
                <div class="stu-preview-title"><?php esc_html_e('Preview Mode', 'subscribe-to-unlock'); ?></div>
            </div>
            <div class="stu-preview-note"><?php esc_html_e('This is just the basic preview and it may look different when used in frontend as per your theme\'s styles.', 'subscribe-to-unlock'); ?></div>
        </div>
        <div class="stu-form-preview-wrap">

            <?php
            echo do_shortcode('[stu alias="' . $form_alias . '"]');
            ?>
        </div>

        <span class="stu-preview-subtitle"><a href="<?php echo admin_url('admin.php?page=subscribe-to-unlock&action=edit_form&form_id=' . esc_attr($form_id)); ?>"><?php esc_html_e('Edit this Form', 'subscribe-to-unlock'); ?></a></span>

        <?php wp_footer(); ?>
    </body>
</html>
