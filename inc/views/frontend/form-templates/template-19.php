<link rel="stylesheet" href="https://fontlibrary.org//face/barlow">
<style>
    .custom-form-template .padding {
        padding: 24px 24px 0 !important;
    }
    .stu-item-flex, .stu-form-wrap .both-fields-wrap, .stu-popup-innerwrap .stu-popup-wrapper {
        display: -webkit-box;
        display: -moz-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: grid !important;
    }
    .stu-form-wrap .stu-field-wrap {
        flex: 1;
        margin-bottom: 10px !important;
    }
    .stu-form-wrap.stu-template-19 .stu-form-submit {
        width: 100% !important;
    }
    .stu-form-wrap.stu-template-19 .stu-btn-parent {
        max-width: 100% !important;
    }
    .stu-field-wrap input {
        border-radius: 5px !important;
    }
    .stu-form-wrap .both-fields-wrap {
        margin-bottom: 0px !important;
    }
    .stu-field-wrap.stu-terms-agreement-wrap.stu-check-box-text {
        margin-bottom: 24px !important;
    }
    .stu-heading-text {
        font-family: 'BarlowRegular', serif !important;
    }
    .custom-form-template * {
        font-family: 'BarlowRegular', serif !important;
    }
</style>
<div class="custom-form-template">
    <div class="padding">
        <?php
        // Heading Text
        if ( $heading_show && !empty( $heading_text ) ) {
            ?>
            <h2 class="stu-heading-text">
                <?php echo $this->sanitize_html( $heading_text ); ?>
            </h2>
            <?php
        }
        ?>
        <?php
        // Sub Heading Text
        if ( $sub_heading_show && !empty( $sub_heading_text ) ) {
            ?>
            <p class="stu-heading-text stu-heading-paragraph"><?php echo $this->sanitize_html( $sub_heading_text ); ?></p>
            <?php
        }
        ?>
        <div class="both-fields-wrap">
            <?php
            // Name Field
            if ( $name_show ) {
                ?>
                <div class="stu-field-wrap name-field">
                    <label for="stu_name" class="sr-only stu-hidden-item"><?php echo esc_attr( $name_label ); ?></label>
                    <input type="text" name="stu_name" class="stu-name" placeholder="<?php echo esc_attr( $name_label ); ?>"/>
                </div>
                <?php
            }
            ?>
            <!--Email Field-->
            <div class="stu-field-wrap">
                <label for="stu_email" class="sr-only stu-hidden-item"><?php echo esc_attr( $email_label ); ?></label>
                <input type="email" name="stu_email" class="stu-email" placeholder="<?php echo esc_attr( $email_label ); ?>"/>
            </div>
            <!-- Email Field-->
            <!--Country Field-->
            <div class="stu-field-wrap">
                <label for="stu_country" class="sr-only stu-hidden-item"><?php echo esc_attr( $country_label ); ?></label>
                <input type="text" name="stu_country" class="stu-country" placeholder="<?php echo esc_attr( $country_label ); ?>"/>
            </div>
            <!-- Country Field-->
            <!--Farm Size Field-->
            <div class="stu-field-wrap">
                <label for="stu_farm_size" class="sr-only stu-hidden-item"><?php echo esc_attr( $farm_size_label ); ?></label>
                <input type="text" name="stu_farm_size" class="stu-farm-size" placeholder="<?php echo esc_attr( $farm_size_label ); ?>"/>
            </div>
            <!-- Farm Size Field-->
            <!-- Subscribe Button-->
            <div class="stu-field-wrap stu-btn-parent">
                <input type="submit" name="stu_form_submit" class="stu-form-submit" value="<?php echo esc_attr( $subscribe_button_text ); ?>"/>
            </div>
        </div>
        <?php
        // Terms and Agreement Text
        if ( $terms_agreement_show && !empty( $terms_agreement_text ) ) {
            ?>
            <div class="stu-field-wrap stu-terms-agreement-wrap stu-check-box-text">
                <label>
                    <input type="checkbox" name="stu_terms_agreement" class="stu-terms-agreement"/>
                    <?php echo $this->sanitize_html( $terms_agreement_text ); ?>
                </label>
            </div>
            <?php
        }
        ?>
    </div>
</div>


<?php
// Footer Text
if ( $footer_show && !empty( $footer_text ) ) {
    ?>
    <div class="stu-footer-text"><?php echo $this->sanitize_html( $footer_text ); ?></div>
    <?php
}
?>
<div class="stu-form-message"></div>
<span class="stu-form-loader-wraper">
        <div class="stu-form-loader stu-form-loader-1"><?php esc_html_e('Loading...','subscribe-to-unlock');?></div>
</span>
