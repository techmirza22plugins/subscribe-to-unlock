<div class="form-contents-wrap">
    <?php
        $icon_image = STU_URL . 'images/lockIcon.png';
    ?>

    <div class="stu-icon-holder">
        <span class="icon">
            <img src="<?php echo esc_url($icon_image); ?>"/>
        </span>
    </div>

    <div class="form-holder">
        <?php
        // Heading Text
        if ( $heading_show && !empty( $heading_text ) ) {
            ?>
            <h2 class="stu-heading-text"><?php echo $this->sanitize_html( $heading_text ); ?></h2>
            <?php
        }
        ?>
        <?php
        // Sub Heading Text
        if ( $sub_heading_show && !empty( $sub_heading_text ) ) {
            ?>
            <p class="stu-heading-text stu-heading-paragraph"><?php echo $this->sanitize_html( $sub_heading_text ); ?></p>
            <?php
        }
        ?>
        <div class="both-fields-wrap">

            <?php
            // Name Field
            if ( $name_show ) {
                ?>
                <div class="stu-field-wrap name-field has-pre-icon">
                    <label for="stu_name" class="sr-only stu-hidden-item"><?php echo esc_attr( $name_label ); ?></label>
                    <input type="text" name="stu_name" class="stu-name" placeholder="<?php echo esc_attr( $name_label ); ?>"/>
                    <i class="fas fa-user"></i>
                </div>
                <?php
            }
            ?>
            <!--Email Field-->
            <div class="stu-field-wrap has-pre-icon">
                <label for="stu_email" class="sr-only stu-hidden-item"><?php echo esc_attr( $email_label ); ?></label>
                <input type="email" name="stu_email" class="stu-email" placeholder="<?php echo esc_attr( $email_label ); ?>"/>
                <i class="far fa-envelope"></i>
            </div>
            <!-- Email Field-->
        </div>

        <?php
        // Terms and Agreement Text
        if ( $terms_agreement_show && !empty( $terms_agreement_text ) ) {
            ?>
            <div class="stu-field-wrap stu-terms-agreement-wrap stu-check-box-text">
                <label>
                    <input type="checkbox" name="stu_terms_agreement" class="stu-terms-agreement"/>
                    <?php echo $this->sanitize_html( $terms_agreement_text ); ?>
                </label>
            </div>
            <?php
        }
        ?>

        <!-- Subscribe Button-->
        <div class="stu-field-wrap stu-btn-parent">
            <input type="submit" name="stu_form_submit" class="stu-form-submit" value="<?php echo esc_attr( $subscribe_button_text ); ?>"/>

        </div>



        <?php
        // Footer Text
        if ( $footer_show && !empty( $footer_text ) ) {
            ?>
            <div class="stu-footer-text"><?php echo $this->sanitize_html( $footer_text ); ?></div>
            <?php
        }
        ?>

        <div class="stu-form-message"></div>
        <span class="stu-form-loader-wraper">
        <div class="stu-form-loader stu-form-loader-1"><?php esc_html_e('Loading...','subscribe-to-unlock');?></div>
    </span>
    </div>
</div>
